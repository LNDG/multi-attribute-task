function Stroop_170907(setup, expInfo)

% 170904 JQK | adapted from http://peterscarfe.com/stroopdemo.html
%            | merged with parts of BasicSoundInputDemo for audio recording
%            | removed parts related to RT encoding by button press
% 170904 JQK | added EEG triggers

%% create individual directory

StroopPath = [setup.subjectsPath, 'Stroop/']; mkdir(StroopPath);

%% set Stroop parameters

expInfo.Stroop.durITI = 1;
expInfo.Stroop.durResp = 2;

%% startup PTB

% Setup PTB with some default values
PsychDefaultSetup(2);

% Perform basic initialization of the sound driver:
InitializePsychSound;

% Open the default audio device [], with mode 2 (== Only audio capture),
% and a required latencyclass of zero 0 == no low-latency mode, as well as
% a frequency of 44100 Hz and 2 sound channels for stereo capture.
% This returns a handle to the audio device:
freq = 44100;
pahandle = PsychPortAudio('Open', [], 2, 0, freq, 2);

% Preallocate an internal audio recording  buffer with a capacity of 5 seconds:
PsychPortAudio('GetAudioData', pahandle, 5);

rng('default');
rng('shuffle');
expInfo.Stroop.seed = rng;

%% for EEG: set up parallel port triggers

if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        daqreset;
        rehash toolboxcache
        dio = digitalio('parallel');
        addline(dio,0:7,0,'out');
    end
end

% Set the screen number to the external secondary monitor if there is one
% connected
displayList = Screen('screens');
displayIdx = displayList(setup.disp);
KbName('UnifyKeyNames'); % Make sure keyboard mapping is the same on all supported operating systems
oldVerbosityLevel = Screen('Preference', 'Verbosity', 2); % show errors and warnings

if setup.DEBUG == 1
    Screen('Preference', 'SkipSyncTests', 1);
    PsychDebugWindowConfiguration(0, setup.opacity);
else
    clear Screen; %PsychDebugWindowConfiguration([], 1);
end

% % Define black, white and grey
% white = WhiteIndex(displayIdx);
% grey = white / 2;
% black = BlackIndex(displayIdx);

screenInfo = openExperiment(50,50,displayIdx); clc; % open drawing canvas
if numel(Screen('screens')) == 1
  HideCursor(screenInfo.curWindow);
end
Screen('TextFont', screenInfo.curWindow, 'Helvetica');
Screen('TextSize', screenInfo.curWindow, 20);
Screen('TextStyle', screenInfo.curWindow, 0); % regular
Screen('TextColor', screenInfo.curWindow, 255); % white
ifi = Screen('GetFlipInterval', screenInfo.curWindow); % Query the frame duration

% initiate black background
Screen('FillRect', screenInfo.curWindow, [255/2 255/2 255/2])

% Flip to clear
Screen('Flip', screenInfo.curWindow);

% Query the frame duration
ifi = Screen('GetFlipInterval', screenInfo.curWindow);

% Set the text size
Screen('TextSize', screenInfo.curWindow, 60);

% Query the maximum priority level
topPriorityLevel = MaxPriority(screenInfo.curWindow);

% Get the centre coordinate of the screenInfo.curWindow
[xCenter, yCenter] = RectCenter(screenInfo.screenRect);

% Set the blend funciton for the screen
Screen('BlendFunction', screenInfo.curWindow, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% Do dummy calls to GetSecs, WaitSecs, KbCheck(-1) to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck(-1);
WaitSecs(0.1);
GetSecs;

%----------------------------------------------------------------------
%                       Timing Information
%----------------------------------------------------------------------

% Interstimulus interval time in seconds and frames
isiTimeSecs = expInfo.Stroop.durITI;
isiTimeFrames = round(isiTimeSecs / ifi);

% Numer of frames to wait before re-drawing
waitframes = 1;

expInfo.Stroop.Timing = [];

%----------------------------------------------------------------------
%                     Colors in words and RGB
%----------------------------------------------------------------------

% We are going to use three colors for this demo. Red, Green and blue.
expInfo.Stroop.wordList = {'Rot', 'Gr�n', 'Blau'};
rgbColors = [255 0 0; 0 255 0; 0 0 255];

% Make the matrix which will determine our condition combinations
condMatrixBase = [sort(repmat([1 2 3], 1, 3)); repmat([1 2 3], 1, 3)];

expInfo.Stroop.trialsPerCondition = 9;

% Duplicate the condition matrix to get the full number of trials
condMatrix = repmat(condMatrixBase, 1, expInfo.Stroop.trialsPerCondition);

% Get the size of the matrix
[~, numTrials] = size(condMatrix);

% Randomise the conditions
%shuffler = Shuffle(1:numTrials);
shuffler = randperm(numTrials);
condMatrixShuffled = condMatrix(:, shuffler);

expInfo.Stroop.ColorCondsByTrial = condMatrixShuffled;
expInfo.Stroop.ColorCondsName = {'Row1: word'; 'Row2: color'};

%----------------------------------------------------------------------
%                       Experimental loop
%----------------------------------------------------------------------

% Animation loop: we loop for the total number of trials
for trial = 1:numTrials

    % Word and color number
    wordNum = condMatrixShuffled(1, trial);
    colorNum = condMatrixShuffled(2, trial);

    % The color word and the color it is drawn in
    theWord = expInfo.Stroop.wordList(wordNum);
    theColor = rgbColors(colorNum, :);

    % If this is the first trial we present a start screen and wait for a
    % key-press
    if trial == 1
        DrawFormattedText(screenInfo.curWindow, 'Benennen Sie bitte die Schriftfarbe. \n\n Dr�cken Sie die rechte Taste, um zu beginnen.',...
            'center', 'center', [0 0 0]);
        vbl = Screen('Flip', screenInfo.curWindow);
        expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'ExpOnset'}, {vbl}];
        if setup.EEG.useEEG
            %% send START parallelTrigger
            if setup.EEG.DIO.parallelTrigger == 1
                % first trigger (1)
                putvalue(dio,0); % set to 0 (trigger off); ~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_1_01 = getvalue(dio); % read out values
                putvalue(dio,1); % on
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_1_11 = getvalue(dio); % read out values
                putvalue(dio,0); % off
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_1_02 = getvalue(dio); % read out values
                % second trigger (2)
                putvalue(dio,0); % set to 0 (trigger off); ~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_2_01 = getvalue(dio); % read out values
                putvalue(dio,2); % on
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_2_11 = getvalue(dio); % read out values
                putvalue(dio,0); % off
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.StartTrigger_2_02 = getvalue(dio); % read out values
            end
        end
        KbStrokeWait;
    end

    % Flip again to sync us to the vertical retrace at the same time as
    % drawing our fixation point
    Screen('DrawDots', screenInfo.curWindow, [xCenter; yCenter], 10, [0 0 0], [], 2);
    if trial == 1
        flipWhen = 0;
    else
        flipWhen = StimOnset+expInfo.Stroop.durResp-ifi/2;
    end
    ITIOnset = Screen('Flip', screenInfo.curWindow, flipWhen);
    expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'FixOnset'}, {ITIOnset}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_01(trial,:) = getvalue(dio);
            % set to on state
            putvalue(dio,16);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_11(trial,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_02(trial,:) = getvalue(dio);
        end
    end
    % set Onset for next stimulus
    flipWhen = ITIOnset+expInfo.Stroop.durITI-ifi/2;

    while GetSecs() < ITIOnset+expInfo.Stroop.durITI-100
        pause(.001)
    end

    if trial ~= 1
        % get audio from previous trial
        StroopAudio.audio{trial-1} = PsychPortAudio('GetAudioData', pahandle);
        PsychPortAudio('Stop', pahandle);
        expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'GetAudio'}, {GetSecs()}];
        if setup.EEG.useEEG
            if setup.EEG.DIO.parallelTrigger == 1
                % set to off state
                putvalue(dio,0);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.BlockTrigger_64_01(trial-1,:) = getvalue(dio);
                % set to on state
                putvalue(dio,64);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.BlockTrigger_64_11(trial-1,:) = getvalue(dio);
                % set to off state
                putvalue(dio,0);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.BlockTrigger_64_02(trial-1,:) = getvalue(dio);
            end
        end
        AudioEncoded = 1;
    end
    
    % start next audio recording at specified time
    PsychPortAudio('Start', pahandle, [], flipWhen);
        
    % The person should be asked to respond to either the written word or
    % the color the word is written in.
    % Present the word, start audio recording.
    
    DrawFormattedText(screenInfo.curWindow, char(theWord), 'center', 'center', theColor);
    AudioEncoded = 0;
    StimOnset = Screen('Flip', screenInfo.curWindow, flipWhen);
    expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'StroopStart'}, {StimOnset}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_01(trial,:) = getvalue(dio);
            % set to on state
            putvalue(dio,32);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_11(trial,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_02(trial,:) = getvalue(dio);
        end
    end
    % We retrieve status once to get access to SampleRate:
    if trial == 1
        StroopAudio.s = PsychPortAudio('GetStatus', pahandle);
    end
end

% record last audio response and exit PTB screen
DrawFormattedText(screenInfo.curWindow, 'Sie haben die Stroop-Aufgabe nun beendet. \n\n Vielen Dank!','center', 'center', [0 0 0]);
vbl = Screen('Flip', screenInfo.curWindow, StimOnset+expInfo.Stroop.durResp-ifi/2);
expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'EndScreen'}, {vbl}];
while GetSecs() < StimOnset+expInfo.Stroop.durResp+expInfo.Stroop.durITI-100
    pause(.001);
end
PsychPortAudio('Stop', pahandle);
StroopAudio.audio{trial} = PsychPortAudio('GetAudioData', pahandle);
expInfo.Stroop.Timing = [expInfo.Stroop.Timing; {'GetAudio'}, {GetSecs()}];

if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.BlockTrigger_64_01(trial,:) = getvalue(dio);
        % set to on state
        putvalue(dio,64);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.BlockTrigger_64_11(trial,:) = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.BlockTrigger_64_02(trial,:) = getvalue(dio);
    end
end
PsychPortAudio('Close', pahandle);

%% wait for 3 seconds before sending end EEG trigger

pause(3);
if setup.EEG.useEEG
    %% send END parallelTrigger
    if setup.EEG.DIO.parallelTrigger == 1
        % first trigger
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_6_01 = getvalue(dio);
        % set to on state
        putvalue(dio,6);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_6_11 = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_6_02 = getvalue(dio);
        % second trigger
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_8_01 = getvalue(dio);
        % set to on state
        putvalue(dio,8);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_8_11 = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.EndTrigger_8_02 = getvalue(dio);
    end
end

%% save data and close screen

save([StroopPath,setup.subj(1:4), '_StroopData_',expInfo.StroopRun,'.mat'], 'StroopAudio', 'expInfo');

%KbStrokeWait;
sca;

end