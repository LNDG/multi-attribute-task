%% initialize

if ispc
    pn.root = ['D:',filesep,'Dokumente und Einstellungen',filesep,'guest',filesep,'Desktop',filesep,'StateSwitchMR',filesep,'C_Paradigm',filesep];
else
    disp('Setup no supported.');
    pn.root = ['/Users/kosciessa/Desktop/StateSwitchMR/C_Paradigm/'];
end

pn.CB       = [pn.root, 'checker',filesep]; addpath(pn.CB);
pn.MAT      = [pn.root, 'dotsx',filesep]; addpath(pn.MAT);
pn.SS       = [pn.root, 'StateSwitch',filesep]; addpath(pn.SS);
pn.Stroop   = [pn.root, 'Stroop',filesep]; addpath(pn.Stroop);
addpath(genpath([pn.root, 'functions',filesep]));
addpath(genpath([pn.root, 'helper',filesep]));
if ispc
    addpath(['C:',filesep,'toolbox',filesep,'Psychtoolbox']); % PTB 3.0.11
else
    addpath(genpath('/Users/Shared/Psychtoolbox/')); % PTB 3.0.13 (160606)
end

if ispc
    Screen('Preference', 'SkipSyncTests', 0);
else 
    Screen('Preference', 'SkipSyncTests', 1);
    oldLevel = Screen('Preference', 'Verbosity', 4); % output debugging info
    PsychDebugWindowConfiguration(0,0.3)
end


%% pre-randomize data for MR experiment

for ind1 = 1:2
    for ind2 = 1:3
        for ind3 = 1:99
            if numel(num2str(ind3)) == 1
                ind3_conv = ['0', num2str(ind3)];
            else ind3_conv = num2str(ind3);
            end
            ID = [num2str(ind1), num2str(ind2), ind3_conv]; disp(ID);
            % create randomization
            expInfo = []; expInfo = eval(['StateSwitch_createExpInfo_dynamic']);
            % save randomization
            save([pn.root,'expInfo_MR/', ID, '_expInfo.mat'], 'expInfo');
        end
    end
end

% TO DO: indicate correct keys