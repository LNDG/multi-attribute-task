function StateSwitch_addSurroundingCues_dynamic_170911(expInfo, screenInfo, indRun, indBlock, indTrial)
% adds the surrounding cues for the next flip
    for indAtt = 1:4
        
        if strcmp(expInfo.cuetype, 'woWord')
            CueImg = ['img',filesep, expInfo.MAT.attNames{indAtt},'QwoWord.png'];
        else
            CueImg = ['img',filesep, expInfo.MAT.attNames{indAtt},'Q.png'];
        end
        [cueLoad,~,~] = imread(CueImg);
        cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
        posScale1 = 10;
        posScale2 = 7; % use 7 for MR, use 8 for EEG
        smallIm = [0 0 floor(posScale1*size(cueLoad,2)) floor(posScale2*size(cueLoad,1))];
        x1 = screenInfo.screenRect(3);
        x2 = screenInfo.screenRect(4);
        coords(1,:) = [.5*x1-(1/posScale1)*x1, .5*x2-(1/posScale2)*x2, .5*x1, .5*x2];
        coords(2,:) = [.5*x1, .5*x2-(1/posScale2)*x2, .5*x1+(1/posScale1)*x1, .5*x2];
        coords(3,:) = [.5*x1-(1/posScale1)*x1, .5*x2, .5*x1, .5*x2+(1/posScale2)*x2];
        coords(4,:) = [.5*x1, .5*x2, .5*x1+(1/posScale1)*x1, .5*x2+(1/posScale2)*x2];
        switch indAtt
            case 1
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(1,:));
            case 2
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(2,:));
            case 3
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(3,:));
            case 4
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(4,:));
        end
        if ismember(indAtt, expInfo.AttCuesRun{indRun}{indBlock,indTrial})
            Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object
        end
    end
end