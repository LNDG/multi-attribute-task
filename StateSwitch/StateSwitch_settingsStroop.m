function setup = StateSwitch_settingsStroop(setup)
    % settings for Stroop
    setup.DEBUG                     = 0; 
    setup.opacity                   = .4; 
    setup.keyB                      = 1; % number of available keyboards
    setup.ET.useET                  = 0;
    setup.EEG.useEEG                = 1;
    setup.EEG.DIO.parallelTrigger   = 1;
    setup.EEG.DIO.protocolDynamic   = 0;
    setup.EEG.waitTrigEnc           = .005; % wait 5 ms to ensure encoding of trigger
    setup.MR.useMR                  = 0;
    setup.ET.falsePupilRec          = 'no';
    setup.ET.ELdummymode            = 0;
    setup.disp                      = 1;
end