%% select subset of stimuli for practice, which will not be shown during the task

stimuli_all = load('/Volumes/LNDG/StateSwitch/B_paradigm/StateSwitchMR/C_Paradigm/StateSwitch/stimuli_corrJQK.mat');
stimuli_exp = load('/Volumes/LNDG/StateSwitch/B_paradigm/StateSwitchMR/C_Paradigm/StateSwitch/stimuliSelected.mat');

Cues = {'Tier?'; 'einsilbig?'; 'gerade?'; 'H?'};
WordCategories = {'NoAnimal', 'animal'; 'twoSyllable', 'oneSyllable'; 'uneven', 'even'; 'noH', 'H'}; % yes option is always right, no option left

combs = allcomb([1,2],[1,2],[1,2],[1,2]);

% get the stimuli for each combination
for indComb = 1:size(combs,1)
    stims_all{indComb} = stimuli_all.stimuli.(WordCategories{1,combs(indComb,1)}).(...
        WordCategories{2,combs(indComb,2)}).(...
        WordCategories{3,combs(indComb,3)}).(...
        WordCategories{4,combs(indComb,4)});
    stims_exp{indComb} = stimuli_exp.stimuli.(WordCategories{1,combs(indComb,1)}).(...
        WordCategories{2,combs(indComb,2)}).(...
        WordCategories{3,combs(indComb,3)}).(...
        WordCategories{4,combs(indComb,4)});
    stimPool{indComb} = setxor(stims_all{indComb},stims_exp{indComb});
    amountAvail = numel(stimPool{indComb});
    if amountAvail >= 4
        tmp_random = randperm(numel(stimPool{indComb}));
        stims_prac{indComb} = stimPool{indComb}(tmp_random(1:4));
    else
        % If insufficient new words are available, words from the task have
        % to be repeated during the practice. This should only affect the
        % animal-two syllable-even-H category.
        tmp_random = randperm(numel(stims_exp{indComb}));
        stims_prac{indComb} = [stimPool{indComb};stims_exp{indComb}(tmp_random(1:4-amountAvail))];
    end
    % build new stimulus structure for the practice words
    stimuli.(WordCategories{1,combs(indComb,1)}).(...
        WordCategories{2,combs(indComb,2)}).(...
        WordCategories{3,combs(indComb,3)}).(...
        WordCategories{4,combs(indComb,4)}) = stims_prac{indComb};
end

save('/Volumes/LNDG/StateSwitch/B_paradigm/StateSwitchMR/C_Paradigm/StateSwitch/stimuliSelected_practice.mat','stimuli');

%% control for low-level features (e.g. amount of letters) across categories

for indComb = 1:size(combs,1)
    for indWord = 1:numel(stims_all{indComb})
        NumLetters_all(indComb, indWord) = numel(stims_all{indComb}{indWord});
    end
    for indWord = 1:numel(stims_exp{indComb})
        NumLetters_exp(indComb, indWord) = numel(stims_all{indComb}{indWord});
    end
end

figure;
subplot(1,2,1);
imagesc(NumLetters_all)
subplot(1,2,2);
imagesc(NumLetters_exp)