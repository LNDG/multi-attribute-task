function StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial)
% adds the surrounding cues for the next flip
    for indAtt = 1:4
        CueImg = ['img_vis',filesep, expInfo.attNames{indAtt},'Q.png'];
        [cueLoad,~,~] = imread(CueImg);
        cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
        scaleFactor = 2;
        smallIm = [0 0 floor(size(cueLoad,2)/scaleFactor) floor(size(cueLoad,1)/scaleFactor)];
        switch indAtt 
            case 1
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/scaleFactor) floor(size(cueLoad,1)/scaleFactor)], [0 0 screenInfo.screenRect(3)/2 screenInfo.screenRect(4)/2]);
            case 2
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/scaleFactor) floor(size(cueLoad,1)/scaleFactor)], [screenInfo.screenRect(3)/2 0 screenInfo.screenRect(3) screenInfo.screenRect(4)/2]);
            case 3
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/scaleFactor) floor(size(cueLoad,1)/scaleFactor)], [0 screenInfo.screenRect(4)/2 screenInfo.screenRect(3)/2 screenInfo.screenRect(4)]);
            case 4
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/scaleFactor) floor(size(cueLoad,1)/scaleFactor)], [screenInfo.screenRect(3)/2 screenInfo.screenRect(4)/2 screenInfo.screenRect(3) screenInfo.screenRect(4)]);
        end
        if ismember(indAtt, expInfo.AttCuesRun{indRun}{indBlock,indTrial})
            Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object
        end
    end
end