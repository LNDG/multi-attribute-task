function setup = StateSwitch_settingsResting_MR(setup)
    % settings for resting state eeg
    setup.DEBUG                     = 0; 
    setup.opacity                   = .4; 
    setup.keyB                      = 1;
    setup.ET.useET                  = 0;
    setup.EEG.useEEG                = 0;
    setup.EEG.DIO.parallelTrigger   = 0;
    setup.EEG.DIO.protocolDynamic   = 0;
    setup.EEG.waitTrigEnc           = .005; % wait 5 ms to ensure encoding of trigger
    setup.MR.useMR                  = 1;
    setup.ET.falsePupilRec          = 'no';
    setup.ET.ELdummymode            = 0;
    setup.disp                      = 2;
end