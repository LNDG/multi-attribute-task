%
% Input
%
% expInfo | experiment configuration (struct)
%    subj | identifier of subject (string)
% condSet | conditions (cell array of strings)
%_______________________________________________________________________
%
% 170707    | updated palamedes to take in more inputs
% 170821    | adapted to state switch variant; no more threshold estimation
% 170816    | included RT feedback + reward at end of each block
% 170908    | preallocated dynamic trial presentation
% 170911    | changed cue presentation
% 171023    | update word version to remove peripheral cues and have new settings

function StateSwitch_practice(expInfo, setup)

  %%
  %% sanitize function parameters
  %%
  if nargin == 0
    eval('help dotsExperiment')
    return
  end

  if not(exist('expInfo','var'))
    error('Missing input: expInfo')
  elseif isempty(expInfo) || not(isstruct(expInfo))
    error('Invalid input: expInfo (requires non-empty struct)')
  end

  if not(isfield(expInfo,'deployed'))
    expInfo.deployed = true;
  end
  
  commandwindow;

  %%
  %% for saving behavioural data
  %%

  sessionFile = [setup.subjectsPath, setup.subj, '_Practice_', datestr(now, 'yymmdd_HHMM'), '.mat'];
    
  %%
  %% prepare presentation
  %%
  displayList = Screen('Screens'); % There appears to be a problem in DEBUG mode.
  displayIdx = displayList(setup.disp);
  KbName('UnifyKeyNames'); % use portable key naming scheme
  oldVerbosityLevel = Screen('Preference', 'Verbosity', 2); % show errors and warning

    if setup.DEBUG == 1
        Screen('Preference', 'SkipSyncTests', 1);
        PsychDebugWindowConfiguration(0, setup.opacity);
    else
        clear Screen; %PsychDebugWindowConfiguration([], 1);
    end

  try

    %%
    %% prepare canvas
    %%
    screenInfo = openExperiment(50,50,displayIdx); clc; % open drawing canvas
    if numel(Screen('screens')) == 1 || expInfo.deployed
      HideCursor(screenInfo.curWindow);
    end
    Screen('TextFont', screenInfo.curWindow, 'Helvetica');
    Screen('TextSize', screenInfo.curWindow, 20);
    Screen('TextStyle', screenInfo.curWindow, 0); % regular
    Screen('TextColor', screenInfo.curWindow, 255); % white
    ifi = Screen('GetFlipInterval', screenInfo.curWindow); % Query the frame duration
    
    % get experimenter and subject keyboards
    keyboardIndices = GetKeyboardIndices;
    if setup.keyB == 1 % use primary keyboard for subject and experimenter
        screenInfo.keyboard_sub = keyboardIndices(1);
        screenInfo.keyboard_exp = keyboardIndices(1);
    elseif setup.keyB == 2
        screenInfo.keyboard_sub = min(keyboardIndices);
        screenInfo.keyboard_exp = max(keyboardIndices);
    else disp('Check setup: only 1 or 2 keyboards supported.');
    end
    
    ResultMat = [];
    
    ExperimentProtocol = cell(0);
    ExperimentProtocol = [ExperimentProtocol; {'SessionOnset'}, {GetSecs}, {[]}, {[]}, {[]}, {NaN},{NaN},{NaN} {[]}];

    %% Intro slide

    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, '\n Willkommen bei der StateSwitch Aufgabe \n\n Im folgenden werden Sie Vierecksmuster sehen, die verschiedene Charakteristika aufweisen: \n\n Farbton \n\n Bewegungsrichtung \n\n Gr��e \n\n S�ttigung \n\n\n Zun�chst werden wir die Attribute einzeln bearbeiten. \n\n Bitte geben Sie an, von welcher der beiden Optionen mehr pr�sentiert wurde. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center');
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, '\n Willkommen bei der StateSwitch Aufgabe \n\n Im folgenden werden Ihnen W�rter pr�sentiert, zu denen verschiedene Fragen gestellt werden: \n\n Handelt es sich um ein TIER? \n\n Ist das Wort EINSILBIG? \n\n Ist die Buchstabenanzahl GERADE? \n\n Beinhaltet das Wort mind. ein H? \n\n Zun�chst werden wir die Fragen einzeln �ben. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center');
    elseif strcmp(setup.task, 'visual')
        DrawFormattedText(screenInfo.curWindow, '\n Willkommen bei der StateSwitch Aufgabe \n\n Im folgenden werden Ihnen visuelle Symbole pr�sentiert, zu denen verschiedene Fragen gestellt werden: \n\n Welche FARBE hat es? \n\n Gibt es ein MUSTER? \n\n Gibt es einen RAND? \n\n Welche FORM hat es? \n\n Zun�chst werden wir die Fragen einzeln �ben. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center');
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Example (1/2)');
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
    if resumeSession
        break
    elseif exitSession
        closeExperiment;
        return
    end
    end
    
    %% Instruction 2
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Wir beginnen mit der Farbe. \n\n\n Falls die gr�nen Punkte �berwiegen, dr�cken Sie die linke Taste. \n\n Falls die roten Punkte �berwiegen, d�cken Sie die rechte Taste. \n\n\n Im Anschluss an Ihre Antwort erhalten Sie eine R�ckmeldung. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center');
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Wir beginnen mit der Frage, ob das Wort ein Tier benennt. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'visual')
        DrawFormattedText(screenInfo.curWindow, 'Wir beginnen mit der Frage, welche Farbe das Muster besitzt. \n\n\n Falls GR�N, dr�cken Sie die linke Taste. \n\n Falls ROT, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Coulour --> Start Colour presentation');
    pause(1);
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
        if resumeSession
            break
        elseif exitSession
            closeExperiment;
            return
        end
    end
    
    %% 1st dimension
    
    % intiate parameters
    DisplayInfo.CombPositionByTrial = [];
    DisplayInfo.CombSamples = [];
    ResultMat = [];
    % get a single run containing only the target attribute
    expInfo = [];
    if strcmp(setup.task, 'dynamic')
        expInfo = StateSwitch_createExpInfo_dynamic_TEST(1);
    elseif strcmp(setup.task, 'words')
        expInfo = StateSwitch_createExpInfo_words_TEST(1);
    elseif strcmp(setup.task, 'visual')
        expInfo = StateSwitch_createExpInfo_visual_TEST(1);
    end
    expInfo.deployed = true;
    expInfo.feedback = 1;
    
    for indRun = 1
        Timing.RunInitiation = GetSecs();
        Timing.lastTiming = 'NaN';
        for indBlock = 1
            % initialize state cues of current condition
            if strcmp(setup.task, 'dynamic')
                StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, 1, 1, 1);
            end
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, ['relevante Attribute: ', num2str(expInfo.StateOrderRun{indRun}(indBlock,1))], 'center', 'center');
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;

            % ####### Block cue onset #####
            if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
                flipWhen = Timing.RunInitiation+(indBlock-1)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward-(ifi/2);
            elseif strcmp(expInfo.timing, 'relative')
                if strcmp(Timing.lastTiming, 'NaN')
                    flipWhen = 0;
                elseif strcmp(Timing.lastTiming, 'ITI')
                    flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
                end                
            end
            Timing.BlockInitiation = Screen('Flip', screenInfo.curWindow, flipWhen);
            Timing.lastTiming = 'BlockCue';
            ExperimentProtocol = [ExperimentProtocol; {'BlockInitiation'}, {Timing.BlockInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
            for indTrial = 1:setup.pr.ntrials_single
                Timing.TrialInitiation = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'TrialInitiation'}, {Timing.TrialInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
                if strcmp(setup.task, 'dynamic')
                    % create fixation
                    targets = makeDotTargets(screenInfo, expInfo); % initialize targets
                    %showTargets(screenInfo, targets, 1);
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_dynamic(screenInfo, expInfo, targets, indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                elseif strcmp(setup.task, 'words') || strcmp(setup.task, 'visual')
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_words_visual(screenInfo, expInfo, [], indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                end
            end
        end
    end
    
    Results{1,1} = expInfo;
    Results{2,1} = ExperimentProtocol;
    Results{3,1} = ResultMat;
    Results{4,1} = DisplayInfo;
    
    %% Instruction
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes die Bewegungsrichtung. \n\n\n Falls die Bewegung nach links �berwiegt, dr�cken Sie die linke Taste. \n\n Falls die Bewegung nach rechts �berwiegt, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes folgt die Frage, ob das Wort einsilbig ist. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'visual')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes folgt die Frage, ob das Symbol ein Muster beinhaltet. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);    
    disp('Experimenter input required: Intro Direction --> Start Direction presentation');

    pause(1);

    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
        if resumeSession
            break
        elseif exitSession
            closeExperiment;
            return
        end
    end
    
    %% 2nd dimension
    
    % intiate parameters
    DisplayInfo.CombPositionByTrial = [];
    DisplayInfo.CombSamples = [];
    ResultMat = [];
    % get a single run containing only the target attribute
    expInfo = [];
    if strcmp(setup.task, 'dynamic')
        expInfo = StateSwitch_createExpInfo_dynamic_TEST(2);
    elseif strcmp(setup.task, 'words')
        expInfo = StateSwitch_createExpInfo_words_TEST(2);
    elseif strcmp(setup.task, 'visual')
        expInfo = StateSwitch_createExpInfo_visual_TEST(2);
    end
    expInfo.deployed = true;
    expInfo.feedback = 1;
    
    for indRun = 1
        Timing.RunInitiation = GetSecs();
        Timing.lastTiming = 'NaN';
        for indBlock = 1
            % initialize state cues of current condition
            if strcmp(setup.task, 'dynamic')
                StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, 1, 1, 1);
            end
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, ['relevante Attribute: ', num2str(expInfo.StateOrderRun{indRun}(indBlock,1))], 'center', 'center');
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;

            % ####### Block cue onset #####
            if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
                flipWhen = Timing.RunInitiation+(indBlock-1)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward-(ifi/2);
            elseif strcmp(expInfo.timing, 'relative')
                if strcmp(Timing.lastTiming, 'NaN')
                    flipWhen = 0;
                elseif strcmp(Timing.lastTiming, 'ITI')
                    flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
                end                
            end
            Timing.BlockInitiation = Screen('Flip', screenInfo.curWindow, flipWhen);
            Timing.lastTiming = 'BlockCue';
            ExperimentProtocol = [ExperimentProtocol; {'BlockInitiation'}, {Timing.BlockInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
            for indTrial = 1:setup.pr.ntrials_single
                Timing.TrialInitiation = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'TrialInitiation'}, {Timing.TrialInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
                if strcmp(setup.task, 'dynamic')
                    % create fixation
                    targets = makeDotTargets(screenInfo, expInfo); % initialize targets
                    %showTargets(screenInfo, targets, 1);
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_dynamic(screenInfo, expInfo, targets, indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                elseif strcmp(setup.task, 'words') || strcmp(setup.task, 'visual')
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_words_visual(screenInfo, expInfo, [], indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                end
            end
        end
    end    
    Results{1,2} = expInfo;
    Results{2,2} = ExperimentProtocol;
    Results{3,2} = ResultMat;
    Results{4,2} = DisplayInfo;
    
    %% Instruction
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes die Punktgr��e. \n\n\n Falls die kleineren Punkte �berwiegen, dr�cken Sie die linke Taste. \n\n Falls die gr��eren Punkte �berwiegen, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes folgt die Frage, ob das Wort eine gerade Anzahl an Buchstaben aufweist. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'visual')
        DrawFormattedText(screenInfo.curWindow, 'Als n�chstes folgt die Frage, ob das Symbol einen wei�en Rand besitzt. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Size --> Start Size presentation');

    pause(1);
    
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
    if resumeSession
        break
    elseif exitSession
        closeExperiment;
        return
    end
    end
    
    %% 3rd dimension
    
    % intiate parameters
    DisplayInfo.CombPositionByTrial = [];
    DisplayInfo.CombSamples = [];
    ResultMat = [];
    % get a single run containing only the target attribute
    expInfo = [];
    if strcmp(setup.task, 'dynamic')
        expInfo = StateSwitch_createExpInfo_dynamic_TEST(3);
    elseif strcmp(setup.task, 'words')
        expInfo = StateSwitch_createExpInfo_words_TEST(3);
    elseif strcmp(setup.task, 'visual')
        expInfo = StateSwitch_createExpInfo_visual_TEST(3);
    end
    expInfo.deployed = true;
    expInfo.feedback = 1;
    
    for indRun = 1
        Timing.RunInitiation = GetSecs();
        Timing.lastTiming = 'NaN';
        for indBlock = 1
            % initialize state cues of current condition
            if strcmp(setup.task, 'dynamic')
                StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, 1, 1, 1);
            end
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, ['relevante Attribute: ', num2str(expInfo.StateOrderRun{indRun}(indBlock,1))], 'center', 'center');
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;

            % ####### Block cue onset #####
            if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
                flipWhen = Timing.RunInitiation+(indBlock-1)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward-(ifi/2);
            elseif strcmp(expInfo.timing, 'relative')
                if strcmp(Timing.lastTiming, 'NaN')
                    flipWhen = 0;
                elseif strcmp(Timing.lastTiming, 'ITI')
                    flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
                end                
            end
            Timing.BlockInitiation = Screen('Flip', screenInfo.curWindow, flipWhen);
            Timing.lastTiming = 'BlockCue';
            ExperimentProtocol = [ExperimentProtocol; {'BlockInitiation'}, {Timing.BlockInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
            for indTrial = 1:setup.pr.ntrials_single
                Timing.TrialInitiation = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'TrialInitiation'}, {Timing.TrialInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
                if strcmp(setup.task, 'dynamic')
                    % create fixation
                    targets = makeDotTargets(screenInfo, expInfo); % initialize targets
                    %showTargets(screenInfo, targets, 1);
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_dynamic(screenInfo, expInfo, targets, indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                elseif strcmp(setup.task, 'words') || strcmp(setup.task, 'visual')
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_words_visual(screenInfo, expInfo, [], indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                end
            end
        end
    end
    
    Results{1,3} = expInfo;
    Results{2,3} = ExperimentProtocol;
    Results{3,3} = ResultMat;
    Results{4,3} = DisplayInfo;
    
    %% Instruction
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Zuletzt die S�ttigung. \n\n\n Falls die unges�ttigteren Punkte �berwiegen, dr�cken Sie die linke Taste. \n\n Falls die ges�ttigteren Punkte �berwiegen, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken. \n\n\n Wichtig: Die Symbole sind stellvertretend f�r S�ttigung, es gibt KEINE gelben Vierecke. ', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Zuletzt die Frage, ob das Wort mind. ein H beinhaltet. \n\n\n Falls NEIN, dr�cken Sie die linke Taste. \n\n Falls JA, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'visual')
        DrawFormattedText(screenInfo.curWindow, 'Zuletzt die Frage, nach der Form. \n\n\n Falls KREIS, dr�cken Sie die linke Taste. \n\n Falls RECHTECK, d�cken Sie die rechte Taste. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Luminance --> Start Luminance presentation');

    pause(1);
    
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
    if resumeSession
        break
    elseif exitSession
        closeExperiment;
        return
    end
    end
    
     %% 4th dimension
    
    % intiate parameters
    DisplayInfo.CombPositionByTrial = [];
    DisplayInfo.CombSamples = [];
    ResultMat = [];
    % get a single run containing only the target attribute
    expInfo = [];
    if strcmp(setup.task, 'dynamic')
        expInfo = StateSwitch_createExpInfo_dynamic_TEST(4);
    elseif strcmp(setup.task, 'words')
        expInfo = StateSwitch_createExpInfo_words_TEST(4);
    elseif strcmp(setup.task, 'visual')
        expInfo = StateSwitch_createExpInfo_visual_TEST(4);
    end
    expInfo.deployed = true;
    expInfo.feedback = 1;
    
    for indRun = 1
        Timing.RunInitiation = GetSecs();
        Timing.lastTiming = 'NaN';
        for indBlock = 1
            % initialize state cues of current condition
            if strcmp(setup.task, 'dynamic')
                StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, 1, 1, 1);
            end
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, ['relevante Attribute: ', num2str(expInfo.StateOrderRun{indRun}(indBlock,1))], 'center', 'center');
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;

            % ####### Block cue onset #####
            if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
                flipWhen = Timing.RunInitiation+(indBlock-1)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward-(ifi/2);
            elseif strcmp(expInfo.timing, 'relative')
                if strcmp(Timing.lastTiming, 'NaN')
                    flipWhen = 0;
                elseif strcmp(Timing.lastTiming, 'ITI')
                    flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
                end                
            end
            Timing.BlockInitiation = Screen('Flip', screenInfo.curWindow, flipWhen);
            Timing.lastTiming = 'BlockCue';
            ExperimentProtocol = [ExperimentProtocol; {'BlockInitiation'}, {Timing.BlockInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
            for indTrial = 1:setup.pr.ntrials_single
                Timing.TrialInitiation = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'TrialInitiation'}, {Timing.TrialInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
                if strcmp(setup.task, 'dynamic')
                    % create fixation
                    targets = makeDotTargets(screenInfo, expInfo); % initialize targets
                    %showTargets(screenInfo, targets, 1);
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_dynamic(screenInfo, expInfo, targets, indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                elseif strcmp(setup.task, 'words') || strcmp(setup.task, 'visual')
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_words_visual(screenInfo, expInfo, [], indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                end
            end
        end
    end
    
    Results{1,4} = expInfo;
    Results{2,4} = ExperimentProtocol;
    Results{3,4} = ResultMat;
    Results{4,4} = DisplayInfo;
    
    %% Instruction 1
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Nun kennen Sie alle Attribute. \n\n  Diese werden Ihnen im Folgenden gemeinsam pr�sentiert. \n\n Bitte geben Sie wie zuvor an, welche der beiden Optionen eines Attributs �berwiegend vorhanden ist. \n\n Das relevante Attribut wird Ihnen dabei nach der Pr�sentation pr�sentiert. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Nun kennen Sie alle Attribute. \n\n  Diese werden Ihnen im Folgenden gemeinsam pr�sentiert. \n\n Bitte geben Sie wie zuvor an, welche der beiden Optionen eines Attributs �berwiegend vorhanden ist. \n\n Das relevante Attribut wird Ihnen dabei erst kurz vor der Pr�sentation pr�sentiert. \n\n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Realistic 1/3');

    pause(1);
    
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
    if resumeSession
        break
    elseif exitSession
        closeExperiment;
        return
    end
    end
    
    %% Instruction 1
    
    if strcmp(setup.task, 'dynamic')
        DrawFormattedText(screenInfo.curWindow, 'Vor jedem Durchgang erfahren Sie, welche Attribute abgefragt werden k�nnen. \n\n Dabei werden nun lediglich die Symbole ohne Namen gezeigt. \n\n Sie werden nun nicht mehr erfahren, ob Ihre Antworten korrekt waren. \n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    elseif strcmp(setup.task, 'words')
        DrawFormattedText(screenInfo.curWindow, 'Vor jedem Durchgang erfahren Sie, welche Attribute abgefragt werden k�nnen. \n\n Bitte ENTER Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
    end
    Screen('Flip', screenInfo.curWindow);
    disp('Experimenter input required: Intro Realistic 2/3');

    pause(1);
    
    % wait for experimenter to resume session
    while true
        [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
    if resumeSession
        break
    elseif exitSession
        closeExperiment;
        return
    end
    end
    
    %% Instruction 2
    
    if expInfo.durConf > 0
        DrawFormattedText(screenInfo.curWindow, 'Zudem bitten wir Sie im Anschluss um eine Einsch�tzung, wie sicher Sie sich bei der Entscheidung sind. \n\n  Bitte dr�cken Sie dann die linke Taste, falls sie sich eher unsicher sind. \n\n Bitte dr�cken Sie dann die rechte Taste, falls sie sich eher sicher sind. \n\n\n Bitte rechte Taste zum Fortfahren dr�cken.', 'center', 'center', [255 255 255]);
        Screen('Flip', screenInfo.curWindow);
        disp('Experimenter input required: Intro Realistic 3/3 --> Start Realistic cueing, no feedback');

        pause(1);

        % wait for experimenter to resume session
        while true
            [exitSession, resumeSession] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
        if resumeSession
            break
        elseif exitSession
            closeExperiment;
            return
        end
        end
    end
    
    %% realistic without cue
    
    % intiate parameters
    DisplayInfo.CombPositionByTrial = [];
    DisplayInfo.CombSamples = [];
    ResultMat = [];
    % get a single run containing only the target attribute
    expInfo = [];
    if strcmp(setup.task, 'dynamic')
        expInfo = StateSwitch_createExpInfo_dynamic_TEST(5);
    elseif strcmp(setup.task, 'words')
        expInfo = StateSwitch_createExpInfo_words_TEST(5);
    elseif strcmp(setup.task, 'visual')
        expInfo = StateSwitch_createExpInfo_visual_TEST(5);
    end
    expInfo.deployed = true;
    
    for indRun = 1
        Timing.RunInitiation = GetSecs();
        Timing.lastTiming = 'NaN';
        for indBlock = 1
            % initialize state cues of current condition
            if strcmp(setup.task, 'dynamic')
                StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, 1, 1, 1);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, 1, 1, 1);
            end
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, ['relevante Attribute: ', num2str(expInfo.StateOrderRun{indRun}(indBlock,1))], 'center', 'center');
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;

            % ####### Block cue onset #####
            if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
                flipWhen = Timing.RunInitiation+(indBlock-1)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward-(ifi/2);
            elseif strcmp(expInfo.timing, 'relative')
                if strcmp(Timing.lastTiming, 'NaN')
                    flipWhen = 0;
                elseif strcmp(Timing.lastTiming, 'ITI')
                    flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
                end                
            end
            Timing.BlockInitiation = Screen('Flip', screenInfo.curWindow, flipWhen);
            Timing.lastTiming = 'BlockCue';
            ExperimentProtocol = [ExperimentProtocol; {'BlockInitiation'}, {Timing.BlockInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
            for indTrial = 1:setup.pr.ntrials_conj
                Timing.TrialInitiation = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'TrialInitiation'}, {Timing.TrialInitiation}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {[]}, {[]}];
                if strcmp(setup.task, 'dynamic')
                    % create fixation
                    targets = makeDotTargets(screenInfo, expInfo); % initialize targets
                    %showTargets(screenInfo, targets, 1);
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_dynamic(screenInfo, expInfo, targets, indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                elseif strcmp(setup.task, 'words') || strcmp(setup.task, 'visual')
                    [~, expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
                    StateSwitch_trialPresentation_words_visual(screenInfo, expInfo, [], indRun, indBlock, indTrial, ...
                    ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup);
                end
            end
        end
    end
    
    Results{1,5} = expInfo;
    Results{2,5} = ExperimentProtocol;
    Results{3,5} = ResultMat;
    Results{4,5} = DisplayInfo;
        
     %% save practice files
    
    save(sessionFile, 'Results', 'setup')
    
    pause(1);
    
    %% inform the subject that the experiment is over
    
    DrawFormattedText(screenInfo.curWindow, 'Sie haben das Training nun beendet! \n\n Falls Sie noch Fragen haben, wenden Sie sich bitte an die Versuchsleiter.', 'center', 'center', [255 255 255]);
    Screen('Flip', screenInfo.curWindow);
    
    %% automatically close screen after 5 s
    
    pause(5);
    
%     disp('Experimenter input required: close experiment');
% %     while true
% %       [exitKeyPressed, resumeKeyPressed] = checkKeys_byKeyB(expInfo, screenInfo.keyboard_exp);
% %       if exitKeyPressed || resumeKeyPressed
% %         break
% %       end
% %     end

  catch exception
    getReport(exception) % show stack trace
  end

  closeExperiment; % close drawing canvas
  Screen('Preference', 'Verbosity', oldVerbosityLevel); % restore verbosity

end
