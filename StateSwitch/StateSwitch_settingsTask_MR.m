function setup = StateSwitch_settingsTask_MR(setup)
    % settings for task eeg
    setup.DEBUG                     = 1; 
    setup.opacity                   = .99; 
    setup.keyB                      = 1;
    setup.ET.useET                  = 0;
    setup.EEG.useEEG                = 0;
    setup.EEG.DIO.parallelTrigger   = 0;
    setup.EEG.DIO.protocolDynamic   = 0;
    setup.EEG.waitTrigEnc           = .005; % wait 5 ms to ensure encoding of trigger
    setup.MR.useMR                  = 0;
    setup.MR.dummy                  = 1;
    setup.ET.falsePupilRec          = 'no';
    setup.ET.ELdummymode            = 1;
    setup.disp                      = 2; % resolution is 1024*768
end