% 171023 | located the word cues centrally

function StateSwitch_addSurroundingCues_words_171023(expInfo, screenInfo, indRun, indBlock, indTrial)
    % adds the surrounding cues for the next flip
%     expInfo.Cues = {'Tier?'; 'einsilbig?'; 'gerade?'; 'H?'};
    for indAtt = 1:4
        posScale1 = 10;
        posScale2 = 7; % use 7 for MR, use 8 for EEG
        x1 = screenInfo.screenRect(3);
        x2 = screenInfo.screenRect(4);
        coords(1,:) = [.5*x1-(1/posScale1)*x1, .5*x2-(1/posScale2)*x2, .5*x1, .5*x2];
        coords(2,:) = [.5*x1, .5*x2-(1/posScale2)*x2, .5*x1+(1/posScale1)*x1, .5*x2];
        coords(3,:) = [.5*x1-(1/posScale1)*x1, .5*x2, .5*x1, .5*x2+(1/posScale2)*x2];
        coords(4,:) = [.5*x1, .5*x2, .5*x1+(1/posScale1)*x1, .5*x2+(1/posScale2)*x2];
%         switch indAtt
%             case 1
%                 smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(1,:));
%             case 2
%                 smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(2,:));
%             case 3
%                 smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(3,:));
%             case 4
%                 smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], coords(4,:));
%         end
        if ismember(indAtt, expInfo.AttCuesRun{indRun}{indBlock,indTrial})
            oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
            DrawFormattedText(screenInfo.curWindow, expInfo.Cues{indAtt}, 'center', 'center', [], [], [],[],[],[],coords(indAtt,:));
            Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;
        end
    end
end