function [frames,expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
    StateSwitch_trialPresentation_dynamic(screenInfo,expInfo,targets,indRun,indBlock,indTrial,ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup,dio)

% DOTSX display dots or targets on screen
%%
% For information on minimum fields of screenInfo and expInfo arguments, see
% also openExperiment and createDotInfo. The input argument - "targets" is not
% necessary unless showing targets with the dots. Since rex only likes integers, 
% almost everything is in visual degrees * 10.
%
%   expInfo.coh             vertical vectors, dots coherence (0...999) for each 
%                           dot patch
%   expInfo.speed           vertical vectors, dots speed (10th deg/sec) for each 
%                           dot patch
%   expInfo.dir             vertical vectors, dots direction (degrees) for each 
%                           dot patch
%   expInfo.dotSize         size of dots in pixels, same for all patches
%   expInfo.movingDotColor  color of dots in RGB for moving dots, same for all patches
%   expInfo.randomDotColor  color of dots in RGB for random dots, same for all patches
%   expInfo.maxDotsPerFrame determined by testing video card
%   expInfo.apXYD           x, y coordinates, and diameter of aperture(s) in 
%                           visual degrees          
%   expInfo.maxDotTime      optional to set maximum duration (sec). If not provided, 
%                           dot presentation is terminated only by user response
%   expInfo.trialtype       1 fixed duration, 2 reaction time
%   expInfo.keys            a set of keyboard buttons that can terminate the 
%                           presentation of dots (optional)
%   expInfo.mouse           a set of mouse buttons that can terminate the 
%                           presentation of dots (optional)
%
%   screenInfo.curWindow    window pointer on which to plot dots
%   screenInfo.center       center of the screen in pixels
%   screenInfo.ppd          pixels per visual degree
%   screenInfo.monRefresh   monitor refresh value
%   screenInfo.dontclear    If set to 1, flip will not clear the framebuffer 
%                           after Flip - this allows incremental drawing of 
%                           stimuli. Needs to be zero for dots to be erased.
%   screenInfo.rseed        random # seed, can be empty set[] 
%
%   targets.rects           dimensions for drawOval
%   targets.colors          color of targets
%   targets.show            optional, if only showing certain targets but don't
%                           want to change targets structure (index number of 
%                           targets) to be shown during dots

% DisplayInfo   | contains info on which conjunction of dots was
%                   presented where
% Timing        | contains info on various event timings

% Algorithm:
%   All calculations take place within a square aperture in which the dots are 
% shown. The dots are constructed in 3 sets that are plotted in sequence.  For 
% each set, the probability that a dot is replotted in motion -- as opposed to 
% randomly replaced -- is given by the expInfo.coh value. This routine generates 
% a set of dots as an (ndots,2) matrix of locations, and then plots them.  In 
% plotting the next set of dots (e.g., set 2), it prepends the preceding set 
% (e.g., set 1).
%

% created by MKMK July 2006, based on ShadlenDots by MNS, JIG and others
% adapted by JQK 2017

% Structures are not altered in this function, so should not have memory
% problems from matlab creating new structures.

% CURRENTLY THERE IS AN ALMOST ONE SECOND DELAY FROM THE TIME DOTSX IS
% CALLED UNTIL THE DOTS START ON THE SCREEN! THIS IS BECAUSE OF PRIORITY.
% NEED TO EVALUATE WHETHER PRIORITY IS REALLY NECESSARY.

% JQK: Note that you need to be CD'd to the superdirectory, so that the
% image referencing works.

% 170704 JQK |  - added confidence ratings; cntrl+alt as answer
%                   alternatives; cleanup
% 170707 JQK |  - added confidence onset timing
%               - take into consideration ifi in presentation loop
% 170809 JQK |  - adapt to state switching paradigm with block order
% 170814 JQK |  - included triggers, uses flip timing for improved timing
%            |  - changed dot update into function for readability
%            |  - surround cue update separated into function
% 170829 JQK |  - updated to include feedback timing
% 170908 JQK |  - put stimulus creation outside loop
% 170911 JQK |  - add postcuefix, altered cue presentation, arrangement of cues

if nargin < 3
    targets = [];
    showtar = [];
else
    if isfield(targets,'show')
        showtar = targets.show;
    else
        showtar = 1:size(targets.rects,1);
    end
end

curWindow = screenInfo.curWindow;

%% calculate continuous index

indTrain_cont = (indRun-1)*expInfo.blocksPerRun*expInfo.blockLengthDim+...
    (indBlock-1)*expInfo.blockLengthDim+indTrial;

%% restrict keys to be used during experiment

if strcmp(expInfo.confOptions, '2')
    RestrictKeysForKbCheck([expInfo.keyLeft(3), expInfo.keyRight(3), expInfo.keyModifier, ...
        expInfo.keyEscape, expInfo.keyReturn, expInfo.keyPause]);
elseif strcmp(expInfo.confOptions, '4')
    RestrictKeysForKbCheck([expInfo.keyLeft, expInfo.keyRight, expInfo.keyConf1, ...
        expInfo.keyConf2, expInfo.keyConf3, expInfo.keyConf4, expInfo.keyModifier, ...
        expInfo.keyEscape, expInfo.keyReturn, expInfo.keyPause]);
end

%% other initializiation

% set new random seed at each iteration and retain seed
rng(sum(100*clock), 'twister');
[expInfo.curSeed{indRun, indBlock, indTrial}] = rng;

% Query the frame duration
ifi = Screen('GetFlipInterval', curWindow);

% Create the aperture square
%apRect = floor(createTRect(expInfo.apXYD, screenInfo));

apD = expInfo.apXYD(:,3); % diameter of aperture
center = repmat(screenInfo.center,size(expInfo.apXYD(:,1)));

% Change x,y coordinates to pixels (y is inverted - pos on bottom, neg. on top)
center = [center(:,1) + expInfo.apXYD(:,1)/10*screenInfo.ppd center(:,2) - ...
    expInfo.apXYD(:,2)/10*screenInfo.ppd]; % where you want the center of the aperture
center(:,3) = expInfo.apXYD(:,3)/2/10*screenInfo.ppd; % add diameter
d_ppd = floor(apD/10 * screenInfo.ppd);	% size of aperture in pixels
dotSize = expInfo.dotSize; % probably better to leave this in pixels, but not sure

% ndots is the number of dots shown per video frame. Dots will be placed in a 
% square of the size of aperture.
% - Size of aperture = Apd*Apd/100  sq deg
% - Number of dots per video frame = 16.7 dots per sq deg/sec,
% When rounding up, do not exceed the number of dots that can be plotted in a 
% video frame (expInfo.maxDotsPerFrame). maxDotsPerFrame was originally in 
% setupScreen as a field in screenInfo, but makes more sense in createDotInfo as 
% a field in expInfo.
% ndots = min(expInfo.maxDotsPerFrame, ...
%     ceil(16.7 * apD .* apD * 0.01 / screenInfo.monRefresh));

ndots = expInfo.DotsPerFrame;

%% send parallel trigger: reset to 0 before sending of triggers
% Initializing this has the advantage that xx ms are lost only once.

if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        putvalue(dio,0); %~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_17_01(indTrain_cont,:) = getvalue(dio); % fixcue
        %setup.EEG.DIO.TrialTrigger_18_01(indTrain_cont,:) = getvalue(dio); % cue
        setup.EEG.DIO.TrialTrigger_20_01(indTrain_cont,:) = getvalue(dio); % stim
        setup.EEG.DIO.TrialTrigger_24_01(indTrain_cont,:) = getvalue(dio); % acc query
        setup.EEG.DIO.TrialTrigger_32_01(indTrain_cont,:) = getvalue(dio); % conf query
        setup.EEG.DIO.TrialTrigger_64_01(indTrain_cont,:) = getvalue(dio); % iti onset
        setup.EEG.DIO.TrialTrigger_72_01(indTrain_cont,:) = getvalue(dio); % postcuefix onset
        %setup.EEG.DIO.TrialTrigger_129_01(indTrain_cont,:) = getvalue(dio); % stim response
        setup.EEG.DIO.TrialTrigger_130_01(indTrain_cont,:) = getvalue(dio); % acc response
        setup.EEG.DIO.TrialTrigger_133_01(indTrain_cont,:) = getvalue(dio); % conf response
    end
end

%% MAT definition

% 170627: For each feature, the dissociation may differ based on difficulty
% of the stimulus.

for indFeature = 1:4
    expInfo.MAT.ndotsH(1,indFeature) = expInfo.MAT.(['percAtt', num2str(indFeature), 'H']) * ndots;
    expInfo.MAT.ndotsL(1,indFeature) = expInfo.MAT.(['percAtt', num2str(indFeature), 'L']) * ndots;
    % The following is just a sanity check and should not cause problems in
    % the current setup.
    if expInfo.MAT.ndotsL(1,indFeature) > expInfo.MAT.ndotsH(1,indFeature)
        disp('Warning: Check percentages for the likelihood. The second percentage appears to be larger than the first.');
    end
end

% get the a priori defined higher probability option for each feature
curChoices = expInfo.HighProbChoiceRun{indRun}{indBlock, indTrial};
curChoices(isnan(curChoices)) = 1; % only necessary if we test a single condition

% encode info on currently active parameters for each feature
for indAtt = 1:4
    atts.(expInfo.MAT.attNames{indAtt}) = [];
    atts.(expInfo.MAT.attNames{indAtt})(1,1) = expInfo.MAT.(expInfo.MAT.attNames{indAtt})(curChoices(indAtt));
    atts.(expInfo.MAT.attNames{indAtt})(1,2) = expInfo.MAT.(expInfo.MAT.attNames{indAtt})(3-curChoices(indAtt));
    atts.(expInfo.MAT.attNames{indAtt})(2,1) = curChoices(indAtt);
    atts.(expInfo.MAT.attNames{indAtt})(2,2) = 3-curChoices(indAtt);
end

RDMFlipTimeSecs = 1/expInfo.Hz_RDM;
RDMFlipTimeFrames = round(RDMFlipTimeSecs / ifi);

%% get display size (for BG dots)

dontclear = screenInfo.dontclear;

% The main loop
frames = 0;
priorityLevel = MaxPriority(curWindow,'KbCheck');
Priority(priorityLevel);

% Make sure the fixation still on
for i = showtar
    Screen('FillOval',screenInfo.curWindow,targets.colors(i,:),targets.rects(i,:));
end

Screen('DrawingFinished',curWindow,dontclear);

%% get starting parameters

% How dots are presented: 1st group of dots are shown in the first frame, a 2nd 
% group are shown in the second frame, a 3rd group shown in the third frame.
% Then in the next (4th) frame, some percentage of the dots from the 1st frame 
% are replotted according to the speed/direction and coherence. Similarly, the 
% same is done for the 2nd group, etc.

% initialize dot fields
ss{1} = rand(ndots*3, 2); % array of dot positions raw [x,y]
% Divide dots into three sets
Ls = cumsum(ones(ndots,3)) + repmat([0 ndots ndots*2], ... 
    ndots, 1);
%loopi = 1; % loops through the three sets of dots

%% PRESENT FIXCUE

if expInfo.durFixCue > 0
    % create target
    Screen('FillOval', screenInfo.curWindow, targets.colors, targets.rects);
    Screen('DrawingFinished',screenInfo.curWindow, screenInfo.dontclear);
    % show state cues
    StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);
    
    % ######### cueOnset ########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all-(ifi/2);
    else
        if strcmp(Timing.lastTiming, 'BlockCue')
            flipWhen = Timing.BlockInitiation+expInfo.durBlockOnset-(ifi/2);
        elseif strcmp(Timing.lastTiming, 'ITI')
            flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
        end
    end
    Timing.FixCueOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'FixCueOnset'}, {Timing.FixCueOnset}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d FixCueOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,17);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_17_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_17_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###########################

end % fixcue presentation loop

%% PRESENT postcuefix

if expInfo.durPostCueFix > 0

    % create target
    Screen('FillOval', screenInfo.curWindow, targets.colors, targets.rects);
    Screen('DrawingFinished',screenInfo.curWindow, screenInfo.dontclear);
    
    % ######### PostFixCueOnset ########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.FixCue-(ifi/2);
    else
        flipWhen = Timing.FixCueOnset+expInfo.durFixCue-(ifi/2);
    end
    Timing.PostCueFixOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'PostCueFixOnset'}, {Timing.PostCueFixOnset}, {[]}, {[]}, {[]}, {indRun}, {indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d PostCueFixOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,72);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_72_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_72_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###################################

end % fixcue presentation loop
    
%% PRESENT MAT STIMULUS

RDMUpdate = 0;
numOfFlips = 0;
firstFlip = 0;

%% pre-allocate stimuli

combs = allcomb([1,2],[1,2],[1,2],[1,2]); % Note that 1 & 2 refer to the higher/lower prob option here.

% fixed: apD, atts, screenInfo, ndots, ss, Ls
tic
totalNumOfFlips = expInfo.Hz_RDM*expInfo.durPres;
% initiate structures
Disp.lum = cell(size(combs,1), totalNumOfFlips+15);
Disp.color = cell(size(combs,1), totalNumOfFlips+15);
Disp.dirDots = cell(size(combs,1), totalNumOfFlips+15);
Disp.dotSize = cell(size(combs,1), totalNumOfFlips+15);
% update baseline
for indUpdate = 1:totalNumOfFlips+15
    % update later updates
    [L, dot_show, maxVal, Lthis, this_s] = updateDotAssignment(expInfo, apD, atts, screenInfo, ndots, ss{indUpdate}, 1, Ls, d_ppd);
    % For each feature, randomly select higher amount of dots in the first
    % cell and lower amount of dots in the second cell. The randomization
    % is conducted before each flip.
    % Ordering of features: color, direction, size, luminance
    randMat = cell(4,2);
    for indFeature = 1:4
        tmp_randChoice = randperm(ndots); % Note that the seed has been saved, so the process should be replicable.
        if indFeature == 2 && maxVal ~= 0 % direction is already set
            randMat{indFeature,1} = find(L); % L is always the higher probability
            randMat{indFeature,2} = find(~L);
        else 
            randMat{indFeature,1} = tmp_randChoice(1:ceil(expInfo.MAT.ndotsH(1,indFeature))); % round up; might cause slightly more than the requested lower probability
            randMat{indFeature,2} = tmp_randChoice(ceil(expInfo.MAT.ndotsH(1,indFeature))+1:end);
        end
    end
    % For displaying, we need to know the dots of conjunction attributes
    % that have to be drawn. These will be encoded in the randMat structure.
    dotsIdx = cell(1,size(combs,1));
    for indComb = 1:size(combs,1)
        dotsIdx{indComb} = mintersect(randMat{1,combs(indComb,1)},...
            randMat{2,combs(indComb,2)},...
            randMat{3,combs(indComb,3)},...
            randMat{4,combs(indComb,4)});
    end
    %% Draw random dots and fix dot, although nothing is flipped yet    
    % Note that dots that fall outside the circle are simply dropped.
    % This may affect the display in a (hopefully) random way.
    % NaN out-of-circle dots                
    xyDis = dot_show;
    outCircle = sqrt(xyDis(1,:).^2 + xyDis(2,:).^2) + expInfo.dotSize/2 > center(1,3);        
    dots2Display = dot_show;
    dots2Display(:,outCircle) = NaN;
    % get conjunction dots
    for indComb = 1:size(combs,1)
        if numel(dotsIdx{indComb}) > 0
            % referencing: attribute vector(index of higher/lower choice(prob of current comb))
            % combs(indComb, X): for this combination, is feature X higher high or low prob
            % atts.x(2,1): high prob option for feature; atts.X(2,2): low prob option
            if indUpdate == 1
                Disp.lum{indComb}     = expInfo.MAT.saturation(atts.saturation(2,combs(indComb, 4)));
                Disp.color{indComb}   = expInfo.MAT.(['color',num2str(Disp.lum{indComb,1})])(atts.color(2,combs(indComb, 1)),:);
                Disp.dotSize{indComb} = expInfo.MAT.size(atts.size(2,combs(indComb, 3)));
            end
            Disp.dirDots{indComb,indUpdate} = dots2Display(:, dotsIdx{indComb}); % Note that 1 is always the index for the dots of the highest probability direction.
            Disp.dirDots{indComb,indUpdate} = Disp.dirDots{indComb,indUpdate}(:,~any(isnan(Disp.dirDots{indComb,indUpdate}),1)); % remove NaN columns
        end
    end
    % Update the dot position array for the next loop
    ss{indUpdate+1}(Lthis, :) = this_s;
    % save info on when which sample was presented
    DisplayInfo.CombSamples(:,indRun,indBlock, indTrial,indUpdate) = cellfun(@numel, Disp.dirDots(:,indUpdate));
end
% Note that the intersection allocation should be saved for subsequent
% processing as it is unlikely that the dimensions are orthogonal.
% Therefore, they may constitute relevant intertrial variance in the
% presentation (e.g. encoding model). Furthermore, the location
% position can inform the visual sampling process.
DisplayInfo.CombLumByTrial{indRun,indBlock, indTrial} = Disp.lum;
DisplayInfo.CombColByTrial{indRun,indBlock, indTrial} = Disp.color;
DisplayInfo.CombSizeByTrial{indRun,indBlock, indTrial} = Disp.dotSize;
DisplayInfo.CombPositionByTrial{indRun,indBlock, indTrial} = Disp.dirDots;
disp(toc);

%% run the presentation loop

% create target
Screen('FillOval', screenInfo.curWindow, targets.colors, targets.rects);
Screen('DrawingFinished',screenInfo.curWindow, screenInfo.dontclear);

while 1 % loop while presentation time has not been reached
        
    %% After all computations, flip to draw dots from the previous loop. 
    % For the first call, this draws nothing.

    if firstFlip == 0
        % ########## stimulus onset ########
        if strcmp(expInfo.timing, 'absolute')
            flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.PostCueFix-(ifi/2);
        else
            flipWhen = Timing.PostCueFixOnset+expInfo.durPostCueFix-(ifi/2);
        end
        Timing.StimOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
        Timing.ScreenFlip = Timing.StimOnset;
        ExperimentProtocol = [ExperimentProtocol; {'StimOnset'}, {Timing.StimOnset}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
        if setup.ET.useET
            Eyelink('Message', sprintf('Block %d Trial %d Order %d StimOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
        end
        if setup.EEG.useEEG
            if setup.EEG.DIO.parallelTrigger == 1
                % set to on state
                putvalue(dio,20);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.TrialTrigger_20_11(indTrain_cont,:) = getvalue(dio);
                % set to off state
                putvalue(dio,0);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.TrialTrigger_20_02(indTrain_cont,:) = getvalue(dio);
            end
        end
        % ###################################
        firstFlip = 1;
    else
        flipWhen = Timing.ScreenFlip+RDMFlipTimeSecs-(ifi/2);
        Timing.ScreenFlip = Screen('Flip', curWindow,flipWhen);
        RDMUpdate = 1;
    end
    numOfFlips = numOfFlips+1;
                
    if RDMUpdate == 1
        Timing.RDMUpdate = Timing.ScreenFlip;
        ExperimentProtocol = [ExperimentProtocol; {'RDMUpdate'}, {Timing.RDMUpdate}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
        if setup.ET.useET
            Eyelink('Message', sprintf('Block %d Trial %d Order %d RDMUpdate', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
        end
        if setup.EEG.useEEG
            if setup.EEG.DIO.parallelTrigger == 1 && setup.EEG.DIO.protocolDynamic == 1
                putvalue(dio,96); putvalue(dio,0);
            end
        end
    end
    RDMUpdate = 0;
    
    %% Display new dot assignment

    % draw conjunction dots
    if exist('Disp')
        for indComb = 1:size(combs,1)
            if numel(Disp.dirDots{indComb,numOfFlips}) > 0
                Screen('DrawDots',curWindow,Disp.dirDots{indComb,numOfFlips},Disp.dotSize{indComb},Disp.color{indComb},center(1,1:2));
            end
        end
    end
    % Draw targets
    for i = showtar
        Screen('FillOval',screenInfo.curWindow,targets.colors(i,:),targets.rects(i,:));
    end
%     
%     %% Prepare next dots presentation
%     
%     % Tell PTB to get ready while doing computations for next dots presentation
%     Screen('DrawingFinished',curWindow,dontclear);
%     %Screen('BlendFunction', curWindow, GL_ONE, GL_ZERO);
%        

    % break presentation loop if presentation time has passed
    if GetSecs()-Timing.StimOnset >= expInfo.durPres-(ifi/2)
        break;
    end
        
end

% % present state cues
% StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);

% Present the last frame of dots
flipWhen = Timing.ScreenFlip+RDMFlipTimeSecs-(ifi/2);
Timing.ScreenFlip = Screen('Flip',curWindow,flipWhen);
Timing.RDMUpdate = Timing.ScreenFlip;
ExperimentProtocol = [ExperimentProtocol; {'RDMUpdate'}, {Timing.RDMUpdate}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
if setup.ET.useET
    Eyelink('Message', sprintf('Block %d Trial %d Order %d RDMUpdate', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
end
if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1 && setup.EEG.DIO.protocolDynamic ==1
        putvalue(dio,96); putvalue(dio,0);
    end
end

% Erase the last frame of dots, but leave up fixation and targets (if targets 
% are up). Make sure the fixation still on.
% StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);
showTargets(screenInfo,targets,showtar);

%% QUERY RESPONSE

if expInfo.durResp ~= 0
    % present state cues
%     StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);
    % present response query
    if strcmp(expInfo.cuetype, 'woWord')
        CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'woWord.png'];
    else
        CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'.png'];
    end
    [cueLoad,~,~] = imread(CueImg);
    cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
    smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
    Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 

    % ########## AccQuery onset ##########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Pres-(ifi/2);
    else
        flipWhen = Timing.StimOnset+expInfo.durPres-(ifi/2);
    end
    Timing.RespOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'RespOnset'}, {Timing.RespOnset}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d RespOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,24);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_24_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_24_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ####################################

    if expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 1
        correctResp = expInfo.keyLeft;
        wrongResp = expInfo.keyRight;
    elseif expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 2
        correctResp = expInfo.keyRight;
        wrongResp = expInfo.keyLeft;
    end 
    
    % initiate response cue
    KbQueueCreate; KbQueueStart;
    
    accuracy = NaN; kp = NaN; rt_acc = NaN; curSelAcc = NaN;
    while GetSecs()-Timing.RespOnset < expInfo.durResp-(ifi/2) % wait until cuetime is over
        %% check for responses
        [keyIsDown, firstPress, ~, ~, ~] = KbQueueCheck;
        if keyIsDown == 1
            kp = find(firstPress);
            Timing.AccRsp = firstPress(kp); % continuous time
            rt_acc = firstPress(kp)-Timing.RespOnset; % reference to onset of last interval
            % ########## Acc response ##########
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d RT %d  ResponseAcc', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial),rt_acc));
            end
            if setup.EEG.useEEG
                if setup.EEG.DIO.parallelTrigger == 1
                    % set to on state
                    putvalue(dio,130);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_130_11(indTrain_cont,:) = getvalue(dio);
                    % set to off state
                    putvalue(dio,0);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_130_02(indTrain_cont,:) = getvalue(dio);
                end
            end
            % ###################################
            % encode accuracy
            if ismember(kp, correctResp)
                accuracy = 1;
                disp('Correct');
            elseif ismember(kp, wrongResp)
                accuracy = 0;
                disp('Wrong');
            end
            % present state cues
%             StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);
            % update picture to be presented
            if expInfo.highlightChoice == 1 && max(ismember(kp, expInfo.keyLeft))
                curSelAcc = 1;
                if strcmp(expInfo.cuetype, 'woWord')
                    CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'woWord_c',num2str(curSelAcc),'.png'];
                else
                    CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'_c',num2str(curSelAcc),'.png'];
                end
                [cueLoad,~,~] = imread(CueImg);
                cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
                Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object
                Screen('Flip', curWindow,0,dontclear);
            elseif expInfo.highlightChoice == 1 && max(ismember(kp, expInfo.keyRight))
                curSelAcc = 2;
                if strcmp(expInfo.cuetype, 'woWord')
                    CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'woWord_c',num2str(curSelAcc),'.png'];
                else
                    CueImg = ['img', filesep, expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'_c',num2str(curSelAcc),'.png'];
                end
                [cueLoad,~,~] = imread(CueImg);
                cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
                Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object
                Screen('Flip', curWindow,0,dontclear);
            end
            % encode acc response
            ExperimentProtocol = [ExperimentProtocol; {'RespAcc'}, {Timing.AccRsp}, {rt_acc}, {kp}, {accuracy},{indRun}, {indBlock}, {indTrial}, {[]}];
            % pause if pause key was pressed
            if ismember(kp, expInfo.keyPause)
                Timing.Pause = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'Pause'}, {Timing.Pause}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
                if setup.ET.useET
                    Eyelink('Message', sprintf('Block %d Trial %d Order %d Pause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
                end
                Screen('TextColor', screenInfo.curWindow, [255 255 255]);
                DrawFormattedText(screenInfo.curWindow, 'User Pause','center', 'center');
                Screen('Flip', screenInfo.curWindow, 0);
                pause(.2)
                KbWait
                Timing.PauseEnd = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'PauseEnd'}, {Timing.PauseEnd}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
                if setup.ET.useET
                    Eyelink('Message', sprintf('Block %d Trial %d Order %d EndPause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
                end
            end
            % indicate accuracy of choice by textcolor
            if expInfo.feedback == 1
                % add cues
                if strcmp(setup.task, 'dynamic')
%                     StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, 1);
                elseif strcmp(setup.task, 'words')
                    StateSwitch_StateSwitch_addSurroundingCues_dynamic_words(expInfo, screenInfo, indRun, indBlock, 1);
                end
                if accuracy == 1
                    DrawFormattedText(screenInfo.curWindow, 'korrekt', 'center', 'center', [0 255 0]);
                elseif accuracy == 0
                    DrawFormattedText(screenInfo.curWindow, 'falsch', 'center', 'center', [255 0 0]);
                end
                Screen('Flip', curWindow,0,dontclear); clear TextColor;
                Screen('TextColor', screenInfo.curWindow, [255 255 255]);
            end
        end % end of response encode loop
    end
    % encode accuracy query information
    ResultMat(indTrain_cont,1:4) = [expInfo.StateOrderRun{indRun}(indBlock,indTrial), expInfo.targetAttRun{indRun}(indBlock, indTrial), rt_acc, accuracy]; % only encodes last response; go to ExperimentProtocol to check previous responses
else Timing.RespOnset = GetSecs();
end % accuracy response

%% QUERY CONFIDENCE (confidence scale of four options: two left, two right / one left, one right)

if expInfo.durConf ~= 0
    % present state cues
%     StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial)
    % display visual confidence probe
    CueImg = ['img/confidence_',expInfo.confOptions,'.png'];
    [cueLoad,~,~] = imread(CueImg);
    cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
    smallIm = CenterRect([0 0 floor(size(cueLoad,2)/2) floor(size(cueLoad,1)/2)], screenInfo.screenRect);
    Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 
    
    % ########## Conf onset ##########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Resp-(ifi/2);
    else
        flipWhen = Timing.RespOnset+expInfo.durResp-(ifi/2); % This makes only limited sense, as the loop will go as long as this anyways.
    end
    Timing.ConfOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'ConfOnset'}, {Timing.ConfOnset}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d ConfidenceOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,32);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_32_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_32_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###################################
    
    kp = NaN; rt_conf = NaN; conf = NaN; curSelConf = NaN;
    while GetSecs()-Timing.ConfOnset < expInfo.durConf-(ifi/2) % wait until cuetime is over
        %% check for responses
        [keyIsDown, firstPress, ~, ~, ~] = KbQueueCheck;
        if keyIsDown == 1
            kp = find(firstPress);
            Timing.ConfRsp = firstPress(kp); % continuous time
            rt_conf = firstPress(kp)-Timing.ConfOnset; % reference to onset of last interval
            % ########## Conf response ##########
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d RT %d  ResponseConf', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial),rt_conf));
            end
            if setup.EEG.useEEG
                if setup.EEG.DIO.parallelTrigger == 1
                    % set to on state
                    putvalue(dio,133);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_133_11(indTrain_cont,:) = getvalue(dio);
                    % set to off state
                    putvalue(dio,0);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_133_02(indTrain_cont,:) = getvalue(dio);
                end
            end
            % ###################################
            % encode confidence
            if ismember(kp, expInfo.keyConf1)
                conf = 1; disp('1');
            elseif ismember(kp, expInfo.keyConf2)
                conf = 2; disp('2');
            elseif ismember(kp, expInfo.keyConf3)
                conf = 3; disp('3');
            elseif ismember(kp, expInfo.keyConf4)
                conf = 4; disp('4');
            end
            if ~isnan(conf) && expInfo.highlightChoice == 1
                % present state cues
%                 StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, indTrial);
                % highlight chosen confidence option
                curSelConf = conf;
                CueImg = ['img/confidence_',expInfo.confOptions,'_c',num2str(curSelConf),'.png'];
                [cueLoad,~,~] = imread(CueImg);
                cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/2) floor(size(cueLoad,1)/2)], screenInfo.screenRect);
                Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 
                Screen('Flip', curWindow,0,dontclear);
            end
            ExperimentProtocol = [ExperimentProtocol; {'Conf'}, {Timing.ConfRsp}, {rt_acc}, {kp}, {accuracy},{indRun}, {indBlock}, {indTrial}, {conf}];
        end % end of confidence encoding
    end
    ResultMat(indTrain_cont,5) = conf; % only encodes last response; go to ExperimentProtocol to check previous responses
    ResultMat(indTrain_cont,6) = rt_conf;
else Timing.ConfOnset = GetSecs();
end

%% output descriptive summary statistics

if ~isempty(ResultMat)
    SummaryOutput{2,1} = '#misses';
    SummaryOutput{3,1} = 'accuracy';
    SummaryOutput{4,1} = 'rt';
    for indAtt = 1:4
        SummaryOutput{1,indAtt+1} = expInfo.MAT.attNames{indAtt};
        % amount of misses
        SummaryOutput{2,indAtt+1} = sum(isnan(ResultMat(ResultMat(:,2)==indAtt,4)));
        % accuracy of the different categories
        SummaryOutput{3,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,4));
        % rts of the different categories
        SummaryOutput{4,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,3));
        if expInfo.durConf ~= 0
            SummaryOutput{5,1} = 'confidence';
            % confidence of the different categories
            SummaryOutput{5,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,5));
        end
    end
    disp(SummaryOutput)
end

% ############ 'please blink' ITI #########################
if strcmp(expInfo.timing, 'absolute')
    flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Conf-(ifi/2);
elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
    flipWhen = Timing.ConfOnset+expInfo.durConf-(ifi/2);
end
% add cues
if strcmp(setup.task, 'dynamic')
%     StateSwitch_addSurroundingCues_dynamic(expInfo, screenInfo, indRun, indBlock, 1);
elseif strcmp(setup.task, 'words')
    StateSwitch_StateSwitch_addSurroundingCues_dynamic_words(expInfo, screenInfo, indRun, indBlock, 1);
end
DrawFormattedText(screenInfo.curWindow, 'Bitte blinzeln.', 'center', 'center');
Timing.ITIOnset = Screen('Flip', screenInfo.curWindow, flipWhen);
ExperimentProtocol = [ExperimentProtocol; {'ITIOnset'}, {Timing.ITIOnset}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
Timing.lastTiming = 'ITI';
if setup.ET.useET
    Eyelink('Message', sprintf('Block %d Trial %d ITIOnset', indBlock));
end
if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        % set to on state
        putvalue(dio,64);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_64_11(indTrain_cont,:) = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_64_02(indTrain_cont,:) = getvalue(dio);
    end
end
% #########################################################

Priority(0); % reset priority

end % function end

function [L, dot_show, maxVal, Lthis, this_s] = updateDotAssignment(expInfo, apD, atts, screenInfo, ndots, ss, loopi, Ls, d_ppd)
    % On each update, the assignments of the attributes to pixels changes.
    % get point cloud according to direction distribution (no incoherence here)
%     if expInfo.MAT.coherence ~= 0
%         [maxVal,~] = max([expInfo.MAT.percAtt2H, expInfo.MAT.percAtt2L]);
%     else maxVal = 0;
%     end
    maxVal = expInfo.MAT.coherence;
    % update movement field with current direction
    % dxdy is an N x 2 matrix that gives jumpsize in units on 0..1
    % deg/sec * ap-unit/deg * sec/jump = ap-unit/jump
    dxdy_1 = repmat((expInfo.speed/10) * (10/apD) * ...
    (3/screenInfo.monRefresh) * [cos(pi*atts.direction(1,1)/180.0), ...
    -sin(pi*atts.direction(1,1)/180.0)], ndots,1);
    dxdy_2 = repmat((expInfo.speed/10) * (10/apD) * ...
    (3/screenInfo.monRefresh) * [cos(pi*atts.direction(1,2)/180.0), ...
    -sin(pi*atts.direction(1,2)/180.0)], ndots,1);
    % ss is the matrix with 3 sets of dot positions, dots from the last 2 
    %   positions and current dot positions
    % Ls picks out the set (e.g., with 5 dots on the screen at a time, 1:5, 
    %   6:10, or 11:15)
    % Lthis has the dot positions from 3 frames ago, which is what is then
    Lthis  = Ls(:,loopi);
    % Moved in the current loop. This is a matrix of random numbers - starting 
    % positions of dots not moving coherently.
    this_s = ss(Lthis,:);
    % Update the loop pointer
    loopi = loopi+1;
    if loopi == 4
        loopi = 1;
    end
    % Compute new locations, how many dots move coherently
    L = rand(ndots,1) < maxVal;
    % Offset the selected dots
    this_s(L,:) = bsxfun(@plus,this_s(L,:),dxdy_1(L,:));
    if sum(~L) > 0
%             if maxVal ~= 0
%                 this_s(~L,:) = bsxfun(@plus,this_s(~L,:),dxdy_2(~L,:)); % get the opposite direction for the rest
%             else 
            this_s(~L,:) = rand(sum(~L),2);	% get new random locations for the rest
%             end
    end
    % Check to see if any positions are greater than 1 or less than 0 which 
    % is out of the square aperture, and replace with a dot along one of the
    % edges opposite from the direction of motion.
    N = sum((this_s > 1 | this_s < 0)')' ~= 0;
    if sum(N) > 0
        xdir = sin(pi*atts.direction(1,1)/180.0); % Simplify things and just use the primary direction here.
        ydir = cos(pi*atts.direction(1,1)/180.0);
        % Flip a weighted coin to see which edge to put the replaced dots
        if rand < abs(xdir)/(abs(xdir) + abs(ydir))
            this_s(find(N==1),:) = [rand(sum(N),1),(xdir > 0)*ones(sum(N),1)];
        else
            this_s(find(N==1),:) = [(ydir < 0)*ones(sum(N),1),rand(sum(N),1)];
        end
    end
    % Convert for plot
    this_x = floor(d_ppd * this_s);	% pix/ApUnit
    % It assumes that 0 is at the top left, but we want it to be in the 
    % center, so shift the dots up and left, which means adding half of the 
    % aperture size to both the x and y directions.
    dot_show = (this_x - d_ppd/2)';
end