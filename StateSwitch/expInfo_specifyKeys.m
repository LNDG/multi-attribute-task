function expInfo = expInfo_specifyKeys(expInfo)

    %% specify keys
    % Use OS X keyboard naming scheme across all plattforms
    % Otherwise LeftControl/RightControl are not recognized
    KbName('UnifyKeyNames');
    expInfo.keyLeft     = [KbName('LeftControl'), KbName('LeftAlt'), KbName('LeftArrow'), KbName('1!'),KbName('2@'), KbName('b'), KbName('z')];    % left response
    expInfo.keyRight    = [KbName('RightControl'), KbName('RightAlt'), KbName('RightArrow'),KbName('6^'),KbName('7&'),KbName('g'), KbName('r')];  % right response
    expInfo.keyConf1    = [KbName('LeftControl'), KbName('LeftArrow'), KbName('1!'),KbName('b')];       % lowest confidence
    expInfo.keyConf2    = [KbName('LeftAlt'), KbName('2@'),KbName('z')];                                % intermediate confidence low
    expInfo.keyConf3    = [KbName('RightAlt'),KbName('6^'),KbName('g')];                                % intermediate confidence high
    expInfo.keyConf4    = [KbName('RightControl'), KbName('RightArrow'),KbName('7&'), KbName('r')];      % highest confidence
    expInfo.keyModifier = KbName('LeftAlt'); % to prevent accidental input
    expInfo.keyEscape   = KbName('Escape'); %
    expInfo.keyReturn   = [KbName('Return'), KbName('7&'), KbName('g'), KbName('r')]; % continue experiment
    expInfo.keyPause    = KbName('p');
    
    % g,r - left; b,z- right (blue does not work)
    
end