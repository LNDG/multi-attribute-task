function StateSwitch_resting(setup)

    expInfo.resting.states = {'EO'; 'EC'};
    expInfo.resting.durEyesOpen = 3*60;
    expInfo.resting.durEyesClosed = 3*60;

    %% for EEG: set up parallel port triggers

    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            daqreset;
            rehash toolboxcache
            dio = digitalio('parallel');
            addline(dio,0:7,0,'out');
        end
    end
    
    %% set up PTB

    displayList = Screen('screens');
    displayIdx = displayList(setup.disp);
    KbName('UnifyKeyNames'); % Make sure keyboard mapping is the same on all supported operating systems
    oldVerbosityLevel = Screen('Preference', 'Verbosity', 2); % show errors and warnings
  
    if setup.DEBUG == 1
        Screen('Preference', 'SkipSyncTests', 1);
        PsychDebugWindowConfiguration(0, setup.opacity);
    else
        clear Screen; %PsychDebugWindowConfiguration([], 1);
    end

  try

    %% prepare canvas

    screenInfo = openExperiment(50,50,displayIdx); clc; % open drawing canvas
    if numel(Screen('screens')) == 1
      HideCursor(screenInfo.curWindow);
    end
    Screen('TextFont', screenInfo.curWindow, 'Helvetica');
    Screen('TextSize', screenInfo.curWindow, 20);
    Screen('TextStyle', screenInfo.curWindow, 0); % regular
    Screen('TextColor', screenInfo.curWindow, 255); % white
    ifi = Screen('GetFlipInterval', screenInfo.curWindow); % Query the frame duration

    % set up experimenter and subject keyboards
    keyboardIndices = GetKeyboardIndices;
    if setup.keyB == 1 % use primary keyboard for subject and experimenter
        screenInfo.keyboard_sub = keyboardIndices(1);
        screenInfo.keyboard_exp = keyboardIndices(1);
    elseif setup.keyB == 2
        screenInfo.keyboard_sub = min(keyboardIndices);
        screenInfo.keyboard_exp = max(keyboardIndices);
    else disp('Check setup: only 1 or 2 keyboards supported.');
    end
    
    % Do dummy calls to GetSecs, WaitSecs, KbCheck(-1) to make sure
    % they are loaded and ready when we need them - without delays
    % in the wrong moment:
    KbCheck(-1);
    WaitSecs(0.1);
    GetSecs;
        
    %% session start info

    if setup.EEG.useEEG
        %% send START parallelTrigger
        if setup.EEG.DIO.parallelTrigger == 1
            % first trigger (1)
            putvalue(dio,0); % set to 0 (trigger off); ~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_1_01 = getvalue(dio); % read out values
            putvalue(dio,1); % on
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_1_11 = getvalue(dio); % read out values
            putvalue(dio,0); % off
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_1_02 = getvalue(dio); % read out values
            % second trigger (2)
            putvalue(dio,0); % set to 0 (trigger off); ~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_2_01 = getvalue(dio); % read out values
            putvalue(dio,2); % on
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_2_11 = getvalue(dio); % read out values
            putvalue(dio,0); % off
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.StartTrigger_2_02 = getvalue(dio); % read out values
        end
    end
    
    % put Ready... on the screen and wait for user press
    DrawFormattedText(screenInfo.curWindow, 'Bereit? \n\n Bitte die rechte Taste dr�cken.', 'center', 'center');
    Screen('Flip', screenInfo.curWindow);
    KbStrokeWait;
    
    if setup.MR.useMR % wait for scanner pulse to start exp
        DrawFormattedText(screenInfo.curWindow, 'Waiting for scanner pulse','center','center', [255 255 255])
        Screen('Flip', screenInfo.curWindow);
        try
            scannerPort     = 888;
            triggerSwitches = 17; % TR of experiment onset; wait for 17th TR (at TR of .625s, this should allow for 10s of equilibration)
            [SCAN t0] = fMRI_waitScannerTriggers(scannerPort, 1, triggerSwitches, []);
            t0 = GetSecs;
            Eyelink('Message', sprintf('Run%d fMRI scanning Start time %g', 1, t0));
        catch
            warning('MR Triggers not working!!')
        end
    else
        t0 = GetSecs;
    end
    
    Timing.sessStartTime = t0;
    ExperimentProtocol = cell(0);
    ExperimentProtocol = [ExperimentProtocol; {'SessionOnset'}, {Timing.sessStartTime}, {[]}, {[]},{[]},{[]},{[]},{NaN},{[]}];
    
    % initiate black background (necessary if eyetracker changed it)
    Screen('FillRect', screenInfo.curWindow, [0 0 0])

    %% present eyes open with fixcross
    
    oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
    DrawFormattedText(screenInfo.curWindow, 'Bitte Augen ge�ffnet lassen und auf das Fixationskreuz schauen.', 'center', 'center');
    Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;
    
    Timing.Intro1 = Screen('Flip', screenInfo.curWindow, 0);
    ExperimentProtocol = [ExperimentProtocol; {'Intro1'}, {Timing.Intro1}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_01 = getvalue(dio);
            % set to on state
            putvalue(dio,16);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_16_02 = getvalue(dio);
        end
    end
    
    oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
    DrawFormattedText(screenInfo.curWindow, '+', 'center', 'center');
    Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;
    
    flipWhen = Timing.Intro1+3-ifi/2;
    Timing.EO_Onset = Screen('Flip', screenInfo.curWindow, flipWhen);
    ExperimentProtocol = [ExperimentProtocol; {'EO_Onset'}, {Timing.EO_Onset}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_01 = getvalue(dio);
            % set to on state
            putvalue(dio,32);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_32_02 = getvalue(dio);
        end
    end
    
    %% present eyes closed with fixcross
    
    oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
    DrawFormattedText(screenInfo.curWindow, 'Bitte Augen schlie�en.', 'center', 'center');
    Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;
    
    flipWhen = Timing.EO_Onset+expInfo.resting.durEyesOpen-ifi/2;
    Timing.Intro2 = Screen('Flip', screenInfo.curWindow, flipWhen);
    ExperimentProtocol = [ExperimentProtocol; {'Intro2'}, {Timing.Intro2}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_64_01 = getvalue(dio);
            % set to on state
            putvalue(dio,64);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_64_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_64_02 = getvalue(dio);
        end
    end
    
    oldTextSize = Screen('TextSize', screenInfo.curWindow, 30);
    DrawFormattedText(screenInfo.curWindow, 'Bitte Augen schlie�en.', 'center', 'center');
    Screen('TextSize', screenInfo.curWindow, oldTextSize); clear oldTextSize;
    
    flipWhen = Timing.Intro2+3-ifi/2;
    Timing.EC_Onset = Screen('Flip', screenInfo.curWindow, flipWhen);
    ExperimentProtocol = [ExperimentProtocol; {'EC_Onset'}, {Timing.EC_Onset}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_128_01 = getvalue(dio);
            % set to on state
            putvalue(dio,128);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_128_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_128_02 = getvalue(dio);
        end
    end
    
    DrawFormattedText(screenInfo.curWindow, ['Sie haben das Experiment beendet. \n\n Wir speichern nun die Signale.'], 'center', 'center');
    flipWhen = Timing.EC_Onset+expInfo.resting.durEyesClosed-ifi/2;
    Timing.EC_Offset = Screen('Flip', screenInfo.curWindow, flipWhen);
    ExperimentProtocol = [ExperimentProtocol; {'EC_Offset'}, {Timing.EC_Offset}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}, {[]}];
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_129_01 = getvalue(dio);
            % set to on state
            putvalue(dio,129);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_129_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.BlockTrigger_129_02 = getvalue(dio);
        end
    end

    %% save results and wait for 5s to send end trigger
    
    save([setup.subjectsPath, setup.subj, '_Rest.mat'], 'ExperimentProtocol', 'setup', 'expInfo')
    
    while GetSecs() < Timing.EC_Offset+5
        pause(.001);
    end
    
    if setup.EEG.useEEG
        %% send END parallelTrigger
        if setup.EEG.DIO.parallelTrigger == 1
            % first trigger
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_6_01 = getvalue(dio);
            % set to on state
            putvalue(dio,6);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_6_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_6_02 = getvalue(dio);
            % second trigger
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_8_01 = getvalue(dio);
            % set to on state
            putvalue(dio,8);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_8_11 = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.EndTrigger_8_02 = getvalue(dio);
        end
    end
          
    % automatically close screen after 1 s
    pause(1);
    Screen('Preference', 'Verbosity', oldVerbosityLevel); % restore verbosity
    sca
    
  catch exception
    getReport(exception) % show stack trace
    Priority(0);
  end % TRY...CATCH
  
end % function