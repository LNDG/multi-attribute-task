load('/Volumes/LNDG/Julian/Projects/StateSwitch/C_Paradigm/StateSwitch/stimuli_corrJQK.mat')

info.combs = allcomb([1,2],[1,2],[1,2],[1,2]);
info.WordCategories = {'NoAnimal', 'animal'; 'twoSyllable', 'oneSyllable'; 'uneven', 'even'; 'noH', 'H'}; % yes option is always right, no option left

rng(170821, 'twister');
info.seedInfo = rng;

% select 4 stimuli from each category

info.numExtract = 4;

for indComb = 1:size(info.combs,1)
    stims{indComb} = stimuli.(info.WordCategories{1,info.combs(indComb,1)}).(...
        info.WordCategories{2,info.combs(indComb,2)}).(...
        info.WordCategories{3,info.combs(indComb,3)}).(...
        info.WordCategories{4,info.combs(indComb,4)});
    % get random order
    info.randomSelection{indComb} = randperm(numel(stims{indComb}));
    % extract first four items
    stimuli.(info.WordCategories{1,info.combs(indComb,1)}).(...
        info.WordCategories{2,info.combs(indComb,2)}).(...
        info.WordCategories{3,info.combs(indComb,3)}).(...
        info.WordCategories{4,info.combs(indComb,4)}) = stims{indComb}(info.randomSelection{indComb}(1:info.numExtract));
end

save('/Volumes/LNDG/Julian/Projects/StateSwitch/C_Paradigm/StateSwitch/stimuliSelected.mat', 'info', 'stimuli')
