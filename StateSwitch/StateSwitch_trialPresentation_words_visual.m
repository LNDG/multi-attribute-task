function [frames,expInfo, ExperimentProtocol,ResultMat,DisplayInfo,Timing] = ...
    StateSwitch_trialPresentation_words_visual(screenInfo,expInfo,targets,indRun,indBlock,indTrial,ExperimentProtocol,ResultMat,DisplayInfo,Timing,setup,dio)

% run trial loop for word version of state switch task

% 170704 JQK |  - added confidence ratings; cntrl+alt as answer
%                   alternatives; cleanup
% 170707 JQK |  - added confidence onset timing
%               - take into consideration ifi in presentation loop
% 170809 JQK |  - adapt to state switching paradigm with block order
% 170814 JQK |  - included triggers, uses flip timing for improved timing
%            |  - changed dot update into function for readability
%            |  - surround cue update separated into function
% 170816 JQK |  - turned into word version
% 170821 JQK |  - added capability for visual version
% 170829 JQK |  - updated to include feedback timing
% 171023 JQK |  - no peripheral cue presentation

targets = []; % no need here
curWindow = screenInfo.curWindow;

%% calculate continuous index

indTrain_cont = (indRun-1)*expInfo.blocksPerRun*expInfo.blockLengthDim+...
    (indBlock-1)*expInfo.blockLengthDim+indTrial;

%% restrict keys to be used during experiment

if strcmp(expInfo.confOptions, '2')
    RestrictKeysForKbCheck([expInfo.keyLeft(3), expInfo.keyRight(3), expInfo.keyModifier, ...
        expInfo.keyEscape, expInfo.keyReturn, expInfo.keyPause]);
elseif strcmp(expInfo.confOptions, '4')
    RestrictKeysForKbCheck([expInfo.keyLeft, expInfo.keyRight, expInfo.keyConf1, ...
        expInfo.keyConf2, expInfo.keyConf3, expInfo.keyConf4, expInfo.keyModifier, ...
        expInfo.keyEscape, expInfo.keyReturn, expInfo.keyPause]);
end

%% other initializiation

% set new random seed at each iteration and retain seed
rng(sum(100*clock), 'twister');
[expInfo.curSeed{indRun, indBlock, indTrial}] = rng;

% Query the frame duration
ifi = Screen('GetFlipInterval', curWindow);

%% send parallel trigger: reset to 0 before sending of triggers
% Initializing this has the advantage that xx ms are lost only once.

if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        putvalue(dio,0); %~20us (undocumented use demo in @dioline\putvalue.m and @digitalio\putvalue.m - args are: uddobj, vals [, lineInds])
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_17_01(indTrain_cont,:) = getvalue(dio); % fixcue
        setup.EEG.DIO.TrialTrigger_18_01(indTrain_cont,:) = getvalue(dio); % cue
        setup.EEG.DIO.TrialTrigger_20_01(indTrain_cont,:) = getvalue(dio); % stim
        %setup.EEG.DIO.TrialTrigger_24_01(indTrain_cont,:) = getvalue(dio); % acc query
        %setup.EEG.DIO.TrialTrigger_32_01(indTrain_cont,:) = getvalue(dio); % conf query
        setup.EEG.DIO.TrialTrigger_64_01(indTrain_cont,:) = getvalue(dio); % iti onset
        setup.EEG.DIO.TrialTrigger_129_01(indTrain_cont,:) = getvalue(dio); % stim response
        %setup.EEG.DIO.TrialTrigger_130_01(indTrain_cont,:) = getvalue(dio); % acc response
        %setup.EEG.DIO.TrialTrigger_133_01(indTrain_cont,:) = getvalue(dio); % conf response
    end
end

%% get display size (for BG dots)

dontclear = screenInfo.dontclear;

% The main loop
frames = 0;
priorityLevel = MaxPriority(curWindow,'KbCheck');
Priority(priorityLevel);

Screen('DrawingFinished',curWindow,dontclear);

%% PRESENT FIXCUE

if expInfo.durFixCue > 0
    % show state cues
    if strcmp(setup.task, 'words')
        StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
    elseif strcmp(setup.task, 'visual')
        StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
    end
    % show fixcross
    DrawFormattedText(curWindow, '+', 'center', 'center');
    
    % ######### cueOnset ########
    if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative')
        if strcmp(Timing.lastTiming, 'BlockCue')
            flipWhen = Timing.BlockInitiation+expInfo.durBlockOnset-(ifi/2);
        elseif strcmp(Timing.lastTiming, 'ITI')
            flipWhen = Timing.ITIOnset+expInfo.durITI-(ifi/2);
        end
    end
    Timing.FixCueOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'CueOnset'}, {Timing.FixCueOnset}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d CueOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,17);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_17_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_17_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###########################
else
    if strcmp(expInfo.timing, 'absolute') || strcmp(expInfo.timing, 'relativeITI')
        Timing.FixCueOnset = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative')
        if strcmp(Timing.lastTiming, 'BlockCue')
            Timing.FixCueOnset = Timing.BlockInitiation+expInfo.durBlockOnset-(ifi/2);
        elseif strcmp(Timing.lastTiming, 'ITI')
            Timing.FixCueOnset = Timing.ITIOnset+expInfo.durITI-(ifi/2);
        end
    end
end % cue presentation loop

%% PRESENT CUE

if expInfo.durCue > 0
    % show state cues
%     if strcmp(setup.task, 'words')
%         StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
%     elseif strcmp(setup.task, 'visual')
%         StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
%     end
    % show cue
    if strcmp(setup.task, 'words')
        curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
        oldTextSize = Screen('TextSize', curWindow, 30);
        DrawFormattedText(curWindow, curCue, 'center', 'center');
        Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
    elseif strcmp(setup.task, 'visual')
        CueImg = ['img_vis', filesep, expInfo.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'Q.png'];
        [cueLoad,~,~] = imread(CueImg);
        cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
        smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
        Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 
    end
            
    % ######### cueOnset ########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.FixCue-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
        flipWhen = Timing.FixCueOnset+expInfo.durFixCue-(ifi/2);
    end
    Timing.CueOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'FixCueOnset'}, {Timing.CueOnset}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d CueOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,18);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_18_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_18_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###########################
else
    if strcmp(expInfo.timing, 'absolute')
        Timing.CueOnset = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.FixCue-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
        Timing.CueOnset = Timing.FixCueOnset+expInfo.durFixCue-(ifi/2);
    end
end % cue presentation loop
    
%% PRESENT word STIMULUS & wait for response

% show state cues
% if strcmp(setup.task, 'words')
%     StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
% elseif strcmp(setup.task, 'visual')
%     StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
% end
if strcmp(setup.task, 'words')
    % show word
    curWord = expInfo.WordsRun{indRun}{indBlock,indTrial};
    curWord = curWord{1};
    oldTextSize = Screen('TextSize', curWindow, 30);
    if strcmp(expInfo.CTsim, 'yes')
        curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
        DrawFormattedText(curWindow, [curCue, '\n\n', curWord], 'center', 'center');
    elseif strcmp(expInfo.CTsim, 'no')
        DrawFormattedText(curWindow, curWord, 'center', 'center');
    end
    Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
elseif strcmp(setup.task, 'visual')
    % draw the object
    combination = [num2str(expInfo.HighProbChoiceRun{indRun}{indBlock, indTrial}(1)), '_', ...
        num2str(expInfo.HighProbChoiceRun{indRun}{indBlock, indTrial}(2)), '_', ...
        num2str(expInfo.HighProbChoiceRun{indRun}{indBlock, indTrial}(3)), '_', ...
        num2str(expInfo.HighProbChoiceRun{indRun}{indBlock, indTrial}(4))];
    CueImg = ['img_vis', filesep, combination,'.png'];
    [cueLoad,~,~] = imread(CueImg);
    cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
    smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
    Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm);
end

 % ########## stimulus onset ########
if strcmp(expInfo.timing, 'absolute')
    flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Cue-(ifi/2);
elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
    flipWhen = Timing.CueOnset+expInfo.durCue-(ifi/2);
end
Timing.StimOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
ExperimentProtocol = [ExperimentProtocol; {'StimOnset'}, {Timing.StimOnset}, {[]}, {[]}, {[]},{indRun}, {indBlock}, {indTrial}, {[]}];
if setup.ET.useET
    Eyelink('Message', sprintf('Block %d Trial %d Order %d StimOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
end
if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        % set to on state
        putvalue(dio,20);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_20_11(indTrain_cont,:) = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_20_02(indTrain_cont,:) = getvalue(dio);
    end
end
% ###################################

%% decide correct response

if expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 1
    correctResp = expInfo.keyLeft;
    wrongResp = expInfo.keyRight;
elseif expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 2
    correctResp = expInfo.keyRight;
    wrongResp = expInfo.keyLeft;
end 

%% collect subject response

% initiate response cue
KbQueueCreate; KbQueueStart;
    
accuracy = NaN; kp = NaN; rt_acc = NaN; curSelAcc = NaN;
while GetSecs()-Timing.StimOnset <= expInfo.durPres-(ifi/2) % wait until cuetime is over
    %% check for responses
    [keyIsDown, firstPress, ~, ~, ~] = KbQueueCheck;
    if keyIsDown == 1 && isnan(kp) % only encode single response
        kp = find(firstPress);
        Timing.AccRsp = firstPress(kp); % continuous time
        rt_acc = firstPress(kp)-Timing.StimOnset; % reference to onset of last interval
        % ########## Acc response ##########
        if setup.ET.useET
            Eyelink('Message', sprintf('Block %d Trial %d Order %d RT %d  ResponseAcc', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial),rt_acc));
        end
        if setup.EEG.useEEG
            if setup.EEG.DIO.parallelTrigger == 1
                % set to on state
                putvalue(dio,129);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.TrialTrigger_129_11(indTrain_cont,:) = getvalue(dio);
                % set to off state
                putvalue(dio,0);
                WaitSecs(setup.EEG.waitTrigEnc);
                setup.EEG.DIO.TrialTrigger_129_02(indTrain_cont,:) = getvalue(dio);
            end
        end
        % ###################################
        % encode accuracy
        if ismember(kp, correctResp)
            accuracy = 1;
            disp('Correct');
        elseif ismember(kp, wrongResp)
            accuracy = 0;
            disp('Wrong');
        end
        % present state cues
%         if strcmp(setup.task, 'words')
%             StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
%         elseif strcmp(setup.task, 'visual')
%             StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
%         end
        % encode acc response
        ExperimentProtocol = [ExperimentProtocol; {'RespAcc'}, {Timing.AccRsp}, {rt_acc}, {kp}, {accuracy},{indRun}, {indBlock}, {indTrial}, {[]}];
        % indicate accuracy of choice by textcolor
        if expInfo.feedback == 1
            if accuracy == 1
                % show state cues
                if strcmp(setup.task, 'words')
                    %StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
                    % show word
                    curWord = expInfo.WordsRun{indRun}{indBlock,indTrial};
                    curWord = curWord{1};
                elseif strcmp(setup.task, 'visual')
                    %StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
                end
                oldTextSize = Screen('TextSize', curWindow, 30);
                if strcmp(expInfo.CTsim, 'yes') && strcmp(setup.task, 'words')
                    curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
                    DrawFormattedText(curWindow, [curCue, '\n\n', curWord], 'center', 'center', [0 255 0]);
                elseif strcmp(expInfo.CTsim, 'no') && strcmp(setup.task, 'words')
                    DrawFormattedText(curWindow, curWord, 'center', 'center', [0 255 0]);
                elseif strcmp(expInfo.CTsim, 'no') && strcmp(setup.task, 'visual')
                    DrawFormattedText(screenInfo.curWindow, 'korrekt', 'center', 'center', [0 255 0]);
                end
                Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
            elseif accuracy == 0
                % show state cues
%                 if strcmp(setup.task, 'words')
%                     StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
%                 elseif strcmp(setup.task, 'visual')
%                     StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
%                 end
                % show word
                curWord = expInfo.WordsRun{indRun}{indBlock,indTrial};
                curWord = curWord{1};
                oldTextSize = Screen('TextSize', curWindow, 30);
                if strcmp(expInfo.CTsim, 'yes') && strcmp(setup.task, 'words')
                    curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
                    DrawFormattedText(curWindow, [curCue, '\n\n', curWord], 'center', 'center', [255 0 0]);
                elseif strcmp(expInfo.CTsim, 'no') && strcmp(setup.task, 'words')
                    DrawFormattedText(curWindow, curWord, 'center', 'center', [255 0 0]);
                elseif strcmp(expInfo.CTsim, 'no') && strcmp(setup.task, 'visual')
                    DrawFormattedText(screenInfo.curWindow, 'falsch', 'center', 'center', [255 0 0]);
                end
                Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
            end
            Screen('Flip', curWindow,0,dontclear); clear TextColor;
            Screen('TextColor', screenInfo.curWindow, [255 255 255]);
        end
        % pause if pause key was pressed
        if ismember(kp, expInfo.keyPause)
            Timing.Pause = GetSecs();
            ExperimentProtocol = [ExperimentProtocol; {'Pause'}, {Timing.Pause}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d Pause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
            end
            Screen('TextColor', screenInfo.curWindow, [255 255 255]);
            DrawFormattedText(screenInfo.curWindow, 'User Pause','center', 'center');
            Screen('Flip', screenInfo.curWindow, 0);
            pause(.2)
            KbWait
            Timing.PauseEnd = GetSecs();
            ExperimentProtocol = [ExperimentProtocol; {'PauseEnd'}, {Timing.PauseEnd}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d EndPause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
            end
        end
    end 
end % end of response encode loop
% encode accuracy query information
ResultMat(indTrain_cont,1:4) = [expInfo.StateOrderRun{indRun}(indBlock,indTrial), expInfo.targetAttRun{indRun}(indBlock, indTrial), rt_acc, accuracy]; % only encodes last response; go to ExperimentProtocol to check previous responses


%% QUERY RESPONSE

if expInfo.durResp ~= 0
    % present state cues
    if strcmp(setup.task, 'words')
        StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
    elseif strcmp(setup.task, 'visual')
        StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
    end
    % present response query
    CueImg = ['img_vis/', expInfo.MAT.attNames{expInfo.targetAttRun{indRun}(indBlock, indTrial)},'.png'];
    [cueLoad,~,~] = imread(CueImg);
    cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
    smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], screenInfo.screenRect);
    Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 

    % ########## AccQuery onset ##########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Pres-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
        flipWhen = Timing.StimOnset+expInfo.durPres-(ifi/2);
    end
    Timing.RespOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'RespOnset'}, {Timing.RespOnset}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d RespOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,24);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_24_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_24_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ####################################

    if expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 1
        correctResp = expInfo.keyLeft;
        wrongResp = expInfo.keyRight;
    elseif expInfo.targetOptionRun{indRun}(indBlock, indTrial) == 2
        correctResp = expInfo.keyRight;
        wrongResp = expInfo.keyLeft;
    end
    
    accuracy = NaN; kp = NaN; rt_acc = NaN; curSelAcc = NaN;
    while GetSecs()-Timing.RespOnset < expInfo.durResp-(ifi/2) % wait until cuetime is over
        %% check for responses
        [keyIsDown, firstPress, ~, ~, ~] = KbQueueCheck;
        if keyIsDown == 1
            kp = find(firstPress);
            Timing.AccRsp = firstPress(kp); % continuous time
            rt_acc = firstPress(kp)-Timing.RespOnset; % reference to onset of last interval
            % ########## Acc response ##########
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d RT %d  ResponseAcc', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial),rt_acc));
            end
            if setup.EEG.useEEG
                if setup.EEG.DIO.parallelTrigger == 1
                    % set to on state
                    putvalue(dio,130);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_130_11(indTrain_cont,:) = getvalue(dio);
                    % set to off state
                    putvalue(dio,0);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_130_02(indTrain_cont,:) = getvalue(dio);
                end
            end
            % ###################################
            % encode accuracy
            if ismember(kp, correctResp)
                accuracy = 1;
                disp('Correct');
            elseif ismember(kp, wrongResp)
                accuracy = 0;
                disp('Wrong');
            end
            % present state cues
            if strcmp(setup.task, 'words')
                StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
            elseif strcmp(setup.task, 'visual')
                StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
            end
            % encode acc response
            ExperimentProtocol = [ExperimentProtocol; {'RespAcc'}, {Timing.AccRsp}, {rt_acc}, {kp}, {accuracy}, {indRun},{indBlock}, {indTrial}, {[]}];
            % pause if pause key was pressed
            if ismember(kp, expInfo.keyPause)
                Timing.Pause = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'Pause'}, {Timing.Pause}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
                if setup.ET.useET
                    Eyelink('Message', sprintf('Block %d Trial %d Order %d Pause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
                end
                Screen('TextColor', screenInfo.curWindow, [255 255 255]);
                DrawFormattedText(screenInfo.curWindow, 'User Pause','center', 'center');
                Screen('Flip', screenInfo.curWindow, 0);
                pause(.2)
                KbWait
                Timing.PauseEnd = GetSecs();
                ExperimentProtocol = [ExperimentProtocol; {'PauseEnd'}, {Timing.PauseEnd}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
                if setup.ET.useET
                    Eyelink('Message', sprintf('Block %d Trial %d Order %d EndPause', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
                end
            end
            if expInfo.feedback == 1
                if accuracy == 1
                    % show state cues
                    if strcmp(setup.task, 'words')
                        StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
                    elseif strcmp(setup.task, 'visual')
                        StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
                    end
                    % show word
                    curWord = expInfo.WordsRun{indRun}{indBlock,indTrial};
                    curWord = curWord{1};
                    oldTextSize = Screen('TextSize', curWindow, 30);
                    if strcmp(expInfo.CTsim, 'yes')
                        curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
                        DrawFormattedText(curWindow, [curCue, '\n\n', curWord], 'center', 'center', [0 255 0]);
                    elseif strcmp(expInfo.CTsim, 'no')
                        DrawFormattedText(curWindow, curWord, 'center', 'center', [0 255 0]);
                    end
                    Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
                elseif accuracy == 0
                    % show state cues
                    if strcmp(setup.task, 'words')
                        StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
                    elseif strcmp(setup.task, 'visual')
                        StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
                    end
                    % show word
                    curWord = expInfo.WordsRun{indRun}{indBlock,indTrial};
                    curWord = curWord{1};
                    oldTextSize = Screen('TextSize', curWindow, 30);
                    if strcmp(expInfo.CTsim, 'yes')
                        curCue = expInfo.Cues{expInfo.targetAttRun{indRun}(indBlock,indTrial)};
                        DrawFormattedText(curWindow, [curCue, '\n\n', curWord], 'center', 'center', [255 0 0]);
                    elseif strcmp(expInfo.CTsim, 'no')
                        DrawFormattedText(curWindow, curWord, 'center', 'center', [255 0 0]);
                    end
                    Screen('TextSize', curWindow, oldTextSize); clear oldTextSize;
                end
                Screen('Flip', curWindow,0,dontclear); clear TextColor;
                Screen('TextColor', screenInfo.curWindow, [255 255 255]);
            end
        end % end of response encode loop
    end
    % encode accuracy query information
    ResultMat(indTrain_cont,1:4) = [expInfo.StateOrderRun{indRun}(indBlock,indTrial), expInfo.targetAttRun{indRun}(indBlock, indTrial), rt_acc, accuracy]; % only encodes last response; go to ExperimentProtocol to check previous responses
else Timing.RespOnset = GetSecs();
end % accuracy response

%% QUERY CONFIDENCE (confidence scale of four options: two left, two right / one left, one right)

if expInfo.durConf ~= 0
    % present state cues
    if strcmp(setup.task, 'words')
        StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
    elseif strcmp(setup.task, 'visual')
        StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
    end
    % display visual confidence probe
    CueImg = ['img/confidence_',expInfo.confOptions,'.png'];
    [cueLoad,~,~] = imread(CueImg);
    cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
    smallIm = CenterRect([0 0 floor(size(cueLoad,2)/2) floor(size(cueLoad,1)/2)], screenInfo.screenRect);
    Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 
    
    % ########## Conf onset ##########
    if strcmp(expInfo.timing, 'absolute')
        flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Resp-(ifi/2);
    elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
        flipWhen = Timing.RespOnset+expInfo.durResp-(ifi/2); % This makes only limited sense, as the loop will go as long as this anyways.
    end
    Timing.ConfOnset = Screen('Flip', curWindow,flipWhen,dontclear); clear flipWhen;
    ExperimentProtocol = [ExperimentProtocol; {'ConfOnset'}, {Timing.ConfOnset}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
    if setup.ET.useET
        Eyelink('Message', sprintf('Block %d Trial %d Order %d ConfidenceOnset', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial)));
    end
    if setup.EEG.useEEG
        if setup.EEG.DIO.parallelTrigger == 1
            % set to on state
            putvalue(dio,32);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_32_11(indTrain_cont,:) = getvalue(dio);
            % set to off state
            putvalue(dio,0);
            WaitSecs(setup.EEG.waitTrigEnc);
            setup.EEG.DIO.TrialTrigger_32_02(indTrain_cont,:) = getvalue(dio);
        end
    end
    % ###################################
    
    kp = NaN; rt_conf = NaN; conf = NaN; curSelConf = NaN;
    while GetSecs()-Timing.ConfOnset < expInfo.durConf-(ifi/2) % wait until cuetime is over
        %% check for responses
        [keyIsDown, firstPress, ~, ~, ~] = KbQueueCheck;
        if keyIsDown == 1
            kp = find(firstPress);
            Timing.ConfRsp = firstPress(kp); % continuous time
            rt_conf = firstPress(kp)-Timing.ConfOnset; % reference to onset of last interval
            % ########## Conf response ##########
            if setup.ET.useET
                Eyelink('Message', sprintf('Block %d Trial %d Order %d RT %d  ResponseConf', indBlock, indTrial, expInfo.StateOrderRun{indRun}(indBlock,indTrial),rt_conf));
            end
            if setup.EEG.useEEG
                if setup.EEG.DIO.parallelTrigger == 1
                    % set to on state
                    putvalue(dio,133);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_133_11(indTrain_cont,:) = getvalue(dio);
                    % set to off state
                    putvalue(dio,0);
                    WaitSecs(setup.EEG.waitTrigEnc);
                    setup.EEG.DIO.TrialTrigger_133_02(indTrain_cont,:) = getvalue(dio);
                end
            end
            % ###################################
            % encode confidence
            if ismember(kp, expInfo.keyConf1)
                conf = 1; disp('1');
            elseif ismember(kp, expInfo.keyConf2)
                conf = 2; disp('2');
            elseif ismember(kp, expInfo.keyConf3)
                conf = 3; disp('3');
            elseif ismember(kp, expInfo.keyConf4)
                conf = 4; disp('4');
            end
            if ~isnan(conf) && expInfo.highlightChoice == 1
                % present state cues
                if strcmp(setup.task, 'words')
                    StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
                elseif strcmp(setup.task, 'visual')
                    StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
                end
                % highlight chosen confidence option
                curSelConf = conf;
                CueImg = ['img/confidence_',expInfo.confOptions,'_c',num2str(curSelConf),'.png'];
                [cueLoad,~,~] = imread(CueImg);
                cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/2) floor(size(cueLoad,1)/2)], screenInfo.screenRect);
                Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object 
                Screen('Flip', curWindow,0,dontclear);
            end
            ExperimentProtocol = [ExperimentProtocol; {'Conf'}, {Timing.ConfRsp}, {rt_acc}, {kp}, {accuracy},{indRun}, {indBlock}, {indTrial}, {conf}];
        end % end of confidence encoding
    end
    ResultMat(indTrain_cont,5) = conf; % only encodes last response; go to ExperimentProtocol to check previous responses
    ResultMat(indTrain_cont,6) = rt_conf;
else Timing.ConfOnset = GetSecs();
end

%% output descriptive summary statistics

if ~isempty(ResultMat)
    SummaryOutput{2,1} = '#misses';
    SummaryOutput{3,1} = 'accuracy';
    SummaryOutput{4,1} = 'rt';
    for indAtt = 1:4
        SummaryOutput{1,indAtt+1} = expInfo.Cues{indAtt};
        % amount of misses
        SummaryOutput{2,indAtt+1} = sum(isnan(ResultMat(ResultMat(:,2)==indAtt,4)));
        % accuracy of the different categories
        SummaryOutput{3,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,4));
        % rts of the different categories
        SummaryOutput{4,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,3));
        if expInfo.durConf ~= 0
            SummaryOutput{5,1} = 'confidence';
            % confidence of the different categories
            SummaryOutput{5,indAtt+1} = nanmean(ResultMat(ResultMat(:,2)==indAtt,5));
        end
    end
    disp(SummaryOutput)
end

% ############ 'please blink' ITI #########################
if strcmp(expInfo.timing, 'absolute')
    flipWhen = Timing.RunInitiation+(indBlock)*expInfo.durBlockOnset+(indBlock-1)*expInfo.blockLengthDim*expInfo.trialDuration.all+(indBlock-1)*expInfo.durReward+(indTrial-1)*expInfo.trialDuration.all+expInfo.trialDuration.Conf-(ifi/2);
elseif strcmp(expInfo.timing, 'relative') || strcmp(expInfo.timing, 'relativeITI')
    flipWhen = Timing.ConfOnset+expInfo.durConf-(ifi/2);
end
% add cues
% if strcmp(setup.task, 'words')
%     StateSwitch_addSurroundingCues_words(expInfo, screenInfo, indRun, indBlock, indTrial);
% elseif strcmp(setup.task, 'visual')
%     StateSwitch_addSurroundingCues_visual(expInfo, screenInfo, indRun, indBlock, indTrial);
% end
DrawFormattedText(screenInfo.curWindow, 'Bitte blinzeln.', 'center', 'center');
Timing.ITIOnset = Screen('Flip', screenInfo.curWindow, flipWhen);
ExperimentProtocol = [ExperimentProtocol; {'ITIOnset'}, {Timing.ITIOnset}, {[]}, {[]}, {[]}, {indRun},{indBlock}, {indTrial}, {[]}];
Timing.lastTiming = 'ITI';
if setup.ET.useET
    Eyelink('Message', sprintf('Block %d Trial %d ITIOnset', indBlock));
end
if setup.EEG.useEEG
    if setup.EEG.DIO.parallelTrigger == 1
        % set to on state
        putvalue(dio,64);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_64_11(indTrain_cont,:) = getvalue(dio);
        % set to off state
        putvalue(dio,0);
        WaitSecs(setup.EEG.waitTrigEnc);
        setup.EEG.DIO.TrialTrigger_64_02(indTrain_cont,:) = getvalue(dio);
    end
end
% #########################################################

Priority(0); % reset priority

end % function end