function setup = StateSwitch_settingsTaskPractice(setup)
    % settings for practice
    setup.DEBUG                     = 0; 
    setup.opacity                   = .99; 
    setup.keyB                      = 1; % number of available keyboards
    setup.ET.useET                  = 0;
    setup.EEG.useEEG                = 0;
    setup.EEG.DIO.parallelTrigger   = 0;
    setup.EEG.DIO.protocolDynamic   = 0;
    setup.EEG.waitTrigEnc           = .005; % wait 5 ms to ensure encoding of trigger
    setup.MR.useMR                  = 0;
    setup.ET.falsePupilRec          = 'no';
    setup.ET.ELdummymode            = 0;
    setup.pr.ntrials_single         = 5; % amount of practice trials: single
    setup.pr.ntrials_conj           = 8; % amount of practice trials: conjunctions
    setup.disp                      = 1;
end