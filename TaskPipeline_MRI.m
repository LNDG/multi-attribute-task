 % Main script for the state switching EEG experiment (Ocober 2017)
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% enter subject ID & task variant %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prompt = {'Enter subject ID:','Enter paradigm (dynamic/words/visual):'};
dlg_title = 'StateSwitch Input';
num_lines = 1;
defaultans = {'0000','dynamic'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

setup.subj = answer{1}; % task materials: 'words', 'visual', 'dynamic'
setup.task = answer{2};

disp(['Continuing with subject ',setup.subj, ' and ', setup.task, ' task version.']);

%% add required paths

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr))
pn.root = pwd;

pn.CB       = fullfile(pn.root, 'checker'); addpath(pn.CB);
pn.MAT      = fullfile(pn.root, 'dotsx'); addpath(pn.MAT);
pn.SS       = fullfile(pn.root, 'StateSwitch'); addpath(pn.SS);
pn.Stroop   = fullfile(pn.root, 'Stroop'); addpath(pn.Stroop);
addpath(genpath(fullfile(pn.root, 'functions')));
addpath(genpath(fullfile(pn.root, 'helper')));

if ispc
    addpath(['C:',filesep,'toolbox',filesep,'Psychtoolbox']); % PTB 3.0.11
else
    addpath(genpath('/Users/Shared/Psychtoolbox/')); % PTB 3.0.13 (160606)
end

%% create results directory

setup.subjectsPath = [pn.root,filesep,'data',filesep, setup.subj '_MR_' datestr(now, 'yymmdd_HHMM') filesep]; mkdir(setup.subjectsPath);

diary([setup.subjectsPath, setup.subj, '_ptbnotes_setup.txt']); version

%% set up PTB

if ispc
    Screen('Preference', 'SkipSyncTests', 0);
else 
    Screen('Preference', 'SkipSyncTests', 1);
    oldLevel = Screen('Preference', 'Verbosity', 4); % output debugging info
    PsychDebugWindowConfiguration(0,0.3)
    % setenv('PSYCH_ALLOW_DANGEROUS', '1');
end

% %% practice task
% 
% questdlg('Ready for Switch Practice?', 'Practice','YES!','YES!');
% 
% cd(pn.root);
% setup = StateSwitch_settingsTaskPractice(setup);
% expInfo = []; expInfo = eval(['StateSwitch_createExpInfo_',setup.task]);
% StateSwitch_practice(expInfo, setup);

%% run state switching task

% questdlg('Ready for Switch Task?', 'Task','YES!','YES!');

prompt = {'Start on run:'};
dlg_title = 'StateSwitch Run Input';
num_lines = 1;
defaultans = {'1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
setup.StartRunOn = str2num(answer{1});

cd(pn.root);
setup = StateSwitch_settingsTask_MR(setup);
% load randomization
load([pn.root, 'expInfo_MR',filesep,setup.subj,'_expInfo.mat'], 'expInfo');
expInfo = expInfo_specifyKeys(expInfo); % update key assignments
expInfo.apXYD = [0 0 260];
expInfo.DotsPerFrame = 288;
StateSwitch_experiment(expInfo, setup)
