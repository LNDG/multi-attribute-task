 % This script can be used to try out the experiment behaviorally (defaults to word version)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% enter subject ID & task variant %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prompt = {'Enter subject ID:','Enter paradigm (dynamic/words/visual):'};
dlg_title = 'StateSwitch Input';
num_lines = 1;
defaultans = {'9999','dynamic'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

setup.subj = answer{1};
setup.task = answer{2}; % task materials: 'words', 'visual', 'dynamic'

disp(['Continuing with subject ',setup.subj, ' and ', setup.task, ' task version.']);

%% add required paths

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr))
pn.root = pwd;

pn.CB           = fullfile(pn.root, 'checker'); addpath(pn.CB);
pn.MAT          = fullfile(pn.root, 'dotsx'); addpath(pn.MAT);
pn.StateSwitch  = fullfile(pn.root, 'StateSwitch'); addpath(pn.StateSwitch);
pn.expInfo       = fullfile(pn.root, 'expInfo');
addpath(genpath(fullfile(pn.root, 'functions')));
addpath(genpath(fullfile(pn.root, 'helper')));

%% specify psychtoolbox path

addpath(genpath(fullfile(pn.root, 'Psychtoolbox')));

%% create results directory

setup.subjectsPath = [pn.root,filesep,'data',filesep, setup.subj '_test_' datestr(now, 'yymmdd_HHMM') filesep]; mkdir(setup.subjectsPath);

diary([setup.subjectsPath, setup.subj, '_ptbnotes_setup.txt']); version

%% pre-randomize data experimental condition data

ind1 = str2num(answer{1}(1));
ind2 = str2num(answer{1}(2));
ind3 = str2num(answer{1}(3:4));
if numel(num2str(ind3)) == 1
    ind3_conv = ['0', num2str(ind3)];
else ind3_conv = num2str(ind3);
end
ID = [num2str(ind1), num2str(ind2), ind3_conv]; disp(ID);
% create randomization
expInfo = []; expInfo = eval(['StateSwitch_createExpInfo_',answer{2}]);
% save randomization
if ~exist(pn.expInfo); mkdir(pn.expInfo); end
save(fullfile(pn.expInfo, [ID, '_expInfo.mat']), 'expInfo');

%% set up PTB

if ispc
    Screen('Preference', 'SkipSyncTests', 0);
else 
    Screen('Preference', 'SkipSyncTests', 1);
    oldLevel = Screen('Preference', 'Verbosity', 4); % output debugging info
    PsychDebugWindowConfiguration(0,0.3)
    % setenv('PSYCH_ALLOW_DANGEROUS', '1');
end

%% run state switching task

prompt = {'Start on run:'};
dlg_title = 'StateSwitch Run Input';
num_lines = 1;
defaultans = {'1'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
setup.StartRunOn = str2num(answer{1});

cd(pn.root);

% settings for trying out the script
setup.DEBUG                     = 1; 
setup.opacity                   = 1; 
setup.keyB                      = 1;
setup.ET.useET                  = 0;    
setup.EEG.useEEG                = 0;
setup.MR.useMR                  = 0;
setup.ET.falsePupilRec          = 'no';
setup.ET.ELdummymode            = 0;
setup.disp                      = 1;

% load randomization
load(fullfile(pn.expInfo, [setup.subj,'_expInfo.mat']), 'expInfo');
expInfo = expInfo_specifyKeys(expInfo); % update key assignments
expInfo.apXYD = [0 0 260];
expInfo.DotsPerFrame = 288;
StateSwitch_experiment(expInfo, setup)
