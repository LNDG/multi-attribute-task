[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14216065.svg)](https://doi.org/10.5281/zenodo.14216065)

## Description

Code was used for the StateSwitch study at the Max Planck Institute for Human Development.
The experiment was implemented in Psychtoolbox for EEG and MRI and is provided without guarantee that it works in any other setting than the one intended.

Results from the experiment have been published in

Kosciessa, J.Q., Lindenberger, U. & Garrett, D.D. Thalamocortical excitability modulation guides human perception under uncertainty. Nature Communications 12, 2430 (2021). https://doi.org/10.1038/s41467-021-22511-7

Kosciessa, J. Q., Mayr, U., Lindenberger, U. & Garrett, D. D. Broadscale dampening of uncertainty adjustment in the aging brain. bioRxiv, 2023.2007.2014.549093 (2023). https://doi.org:10.1101/2023.07.14.549093

### Session components

During the EEG session the following programs were run:

- Resting state (eyes closed, eyes open)
- Stroop task (auditory response)
- MAT practice
- MAT task
- Stoop task (post-task)

During the MRI session, only MAT practice and task were performed.

The random 'square' experiment is based on the coherent random dot code created in the Shadlen lab.
Credit goes to the original code authors (Tony Movshon, Maria Mckinley, Jian Wang et al.).
A copy of the original code can be downloaded from https://shadlenlab.columbia.edu/resources/VCRDM.html

## Setup and Requirements

Stimulus PC

* System:
	* Operating system: Windows 7 32-bit
	* Processor: Intel(R) Core™2 PCU 6700 @ 2.66 GHz
	* RAM: 3.00 GB
	 
* Monitor: AG Neovo X24
	* http://www.agneovo.com/de/content/x-24.asp
	* 6.1.7600.16385 (win7_rtm.090713-1255) (Generic PnP Monitor)
	* 1920*1080
	* 60 Hz
 
* Graphics: 
	* NVIDIA Quadro FX 3450/4000 SDI (2015)
	* Driver Version: 9.18.13.908
 
* Programs:
	* Matlab R2015b (32-bit)
	* PsychToolbox 3.0.11
	* Manual copy of 32-bit eye tracker .mex-files to /Psychtoolbox/PsychBasic/MatlabWindowsFilesR2007a/
	* EyeLink Developer’s Kit 1.11.5.0
	* Eyelink Version: 4.40
	* NVIDIA Graphics Driver 309.08
	* NVIDIA nView 136.53
	* Enabled virtual retrace (right click on Desktop à NVIDIA Control Panel à Mangae 3D Settings à Global Settings à Vertical Sync on)
	* Mingw_get 0.6.2
 
* Devices:
	* Parallelport splitter cable between Stimulus computer, EyeLink 1000 and BrainVision box
	* Buttonbox in cabin with buttons 1:7
	* USB-Microphone

## Maintainer

* [Julian Q. Kosciessa](mailto:kosciessa@mpib-berlin.mpg.de)