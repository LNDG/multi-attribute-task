% Main script for the state switching MR practice (October 2017)
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% enter subject ID & task variant %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prompt = {'Enter subject ID:','Enter paradigm (dynamic/words/visual):'};
dlg_title = 'StateSwitch Input';
num_lines = 1;
defaultans = {'0000','dynamic'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

setup.subj = answer{1}; % task materials: 'words', 'visual', 'dynamic'
setup.task = answer{2};

disp(['Continuing with subject ',setup.subj, ' and ', setup.task, ' task version.']);

%% add required paths

if ispc
    pn.root = ['D:',filesep,'Dokumente und Einstellungen',filesep,'guest',filesep,'Desktop',filesep,'StateSwitchMR',filesep,'C_Paradigm',filesep];
else
    %disp('Setup no supported.');
    pn.root = ['/Users/kosciessa/Desktop/StateSwitchMR/C_Paradigm/'];
end

pn.CB       = [pn.root, 'checker',filesep]; addpath(pn.CB);
pn.MAT      = [pn.root, 'dotsx',filesep]; addpath(pn.MAT);
pn.SS       = [pn.root, 'StateSwitch',filesep]; addpath(pn.SS);
pn.Stroop   = [pn.root, 'Stroop',filesep]; addpath(pn.Stroop);
addpath(genpath([pn.root, 'functions',filesep]));
addpath(genpath([pn.root, 'helper',filesep]));
% if ispc
%     addpath(['C:',filesep,'toolbox',filesep,'Psychtoolbox']); % PTB 3.0.11
% else
%     addpath(genpath('/Users/Shared/Psychtoolbox/')); % PTB 3.0.13 (160606)
% end

addpath(genpath('/Applications/Psychtoolbox/'))

%% create results directory

setup.subjectsPath = [pn.root,filesep,'data',filesep, setup.subj '_MR_' datestr(now, 'yymmdd_HHMM') filesep]; mkdir(setup.subjectsPath);

diary([setup.subjectsPath, setup.subj, '_ptbnotes_setup.txt']); version

%% set up PTB

if ispc
    Screen('Preference', 'SkipSyncTests', 0);
else 
    Screen('Preference', 'SkipSyncTests', 1);
    oldLevel = Screen('Preference', 'Verbosity', 4); % output debugging info
    PsychDebugWindowConfiguration(0,0.3)
    % setenv('PSYCH_ALLOW_DANGEROUS', '1');
end

%% run state switching task

% questdlg('Ready for Switch Practice?', 'Practice','YES!','YES!');

cd(pn.root);
setup = StateSwitch_settingsTaskPractice(setup);
% load randomization
load([pn.root, 'expInfo_MR',filesep,setup.subj,'_expInfo.mat'], 'expInfo');
expInfo = expInfo_specifyKeys(expInfo); % update key assignments
expInfo.apXYD = [0 0 260];
expInfo.DotsPerFrame = 288;
StateSwitch_practice(expInfo, setup)
