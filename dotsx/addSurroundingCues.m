function addSurroundingCues(dotInfo, screenInfo, indRun, indBlock, indTrial)
% adds the surrounding cues for the next flip
    for indAtt = 1:4
        CueImg = ['img/', dotInfo.MAT.attNames{indAtt},'Q.png'];
        [cueLoad,~,~] = imread(CueImg);
        cue2Disp = Screen('MakeTexture',screenInfo.curWindow,cueLoad);
        smallIm = [0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)];
        switch indAtt 
            case 1
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], [0 0 screenInfo.screenRect(3)/2 screenInfo.screenRect(4)/2]);
            case 2
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], [screenInfo.screenRect(3)/2 0 screenInfo.screenRect(3) screenInfo.screenRect(4)/2]);
            case 3
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], [0 screenInfo.screenRect(4)/2 screenInfo.screenRect(3)/2 screenInfo.screenRect(4)]);
            case 4
                smallIm = CenterRect([0 0 floor(size(cueLoad,2)/1.5) floor(size(cueLoad,1)/1.5)], [screenInfo.screenRect(3)/2 screenInfo.screenRect(4)/2 screenInfo.screenRect(3) screenInfo.screenRect(4)]);
        end
        if ismember(indAtt, dotInfo.AttCuesRun{indRun}{indBlock,indTrial})
            Screen('DrawTexture', screenInfo.curWindow, cue2Disp, [], smallIm); % draw the object
        end
    end
end