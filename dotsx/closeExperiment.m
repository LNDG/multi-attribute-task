%_______________________________________________________________________
%
% Shut down the experiment
%_______________________________________________________________________
%
% Close the presentation screen, return priority to normal and re-enable
% the mouse cursor.
%_______________________________________________________________________
%
function closeExperiment
  Priority(0);
  Screen('CloseAll');
  ShowCursor;
end
