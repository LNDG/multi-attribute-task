%_______________________________________________________________________
%
% generate fixation dot
%_______________________________________________________________________
%
% Input
%
%    dotInfo | experiment configuration (struct)
% screenInfo | canvas configuration (struct)
%_______________________________________________________________________
%
% Output
%
% targets | properties of fixation dot (struct)
%_______________________________________________________________________
function targets = makeDotTargets(screenInfo, dotInfo)

  % add fixation to targets
  xpos = dotInfo.fixXY(1);
  ypos = dotInfo.fixXY(2);
  diam = dotInfo.fixDiam;
  colors = dotInfo.fixColor;

  % initialize targets
  targets = setNumTargets(length(xpos));
  targets = newTargets(screenInfo, targets, 1:length(xpos), xpos, ypos, diam, colors);
  targets.select = selectTargets(screenInfo, [xpos' ypos'], diam);

  % fixation is on as long as dots are on
  targets.show = 1;

end
