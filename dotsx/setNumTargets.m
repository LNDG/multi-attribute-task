%_______________________________________________________________________
%
% targets = setNumTargets(numTargets)
%_______________________________________________________________________
%
% creates the targets structure
%_______________________________________________________________________
%
% By default, the number of targets is 8 and the target structure fields
% are  rects, colors, x, y, d.
%_______________________________________________________________________
%
% Input
%
% numTargets | number of targets (numeric)
%_______________________________________________________________________
%
% Output
%
%    targets | target properties (structure)
%_______________________________________________________________________
%
% (c) MKMK July, 2006
%
function targets = setNumTargets(numTargets)

if nargin < 1 || isempty(numTargets)
	numTargets = 8;
end

targets.rects = zeros(numTargets, 4);
targets.colors = zeros(numTargets, 3);
targets.x = zeros(numTargets, 1);
targets.y = zeros(numTargets, 1);
targets.d = zeros(numTargets, 1);
