%_______________________________________________________________________
%
% showTargets(screenInfo, targets, targetIndex)
%_______________________________________________________________________
%
% Displays the targets identified by targetIndex in the structure targets 
%_______________________________________________________________________
%
% Input
%
%  screenInfo | has screen number and other info (from openExperiment)
%     targets | targets are set using newTargets (from setNumTargets)
% targetIndex | number of targets to be shown, if [] or no targetIndex
%               given, erase everything)
%_______________________________________________________________________
%
% Examples
%
% To show a single target (#1) at its current position & color
%
% showTargets(screenInfo, targets, 1)
%
% To change a target, first call newTargets and then showTargets
%
% targets = newTargets(screenInfo,targets,[2 3],[],[],[],[255 255 0; 0 255 255])
% showTargets(screenInfo, targets, [1 2 3])
%_______________________________________________________________________
%
% (c) by MKMK July, 2006
%_______________________________________________________________________
%
function showTargets(screenInfo, targets, targetIndex)

% If no index provided, all targets will be erased
if nargin < 3
    targetIndex = [];
end

% Loop through each target to be shown & draw it
for i = targetIndex
    if ~any(targets.rects(i,:))        
        warning('index %d is not a valid target\n',i);
        return
    end
	Screen('FillOval', screenInfo.curWindow, targets.colors(i,:), targets.rects(i,:));
end

% Draw all the targets
Screen('DrawingFinished',screenInfo.curWindow, screenInfo.dontclear);
Screen('Flip', screenInfo.curWindow, 0, screenInfo.dontclear);
