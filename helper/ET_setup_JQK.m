function el = ET_setup_JQK(setup, el, subNo, phase, irun)

% setup.ET.ELdummymode   | play pretend?
% setup.ET.falsePupilRec | record false pupil?
% el            | eye link parameters
% subNo         | subject number (string)
% phase         | not applicable here, just set to 1
% irun          | index of current run

% ChangeLog:
% 160905 | Eyetracking setup cf Dahl
% XXXXXX | edited by NK, turned into function
% 170814 | JQK script adapted from NK

status = Eyelink('IsConnected'); % try to close ET if it is still opened (prior task etc.)
if status ~= 0
    try
        Eyelink('Shutdown');
    end
end
% Disable key output to Matlab window:
ListenChar(2);

% % % Initialize ET
if ~EyelinkInit(setup.ET.ELdummymode, 1)
    fprintf('Eyelink Init aborted.\n');
    cleanup; % cleanup function
    return;
end

% Check to ensure we're connected to the tracker.
status = Eyelink('IsConnected');
if (status == 0)
    Eyelink('Shutdown');
    Screen('CloseAll');
    fprintf('Lost connection to EyeTracker!\n');
    return;
end

% Check & display tracker version
[v vs]=Eyelink('GetTrackerVersion');
if not(isempty(vs))
    fprintf('Running experiment on a ''%s'' tracker.\n', vs );              % noted in PTB notes
end
vsn = regexp(vs,'\d','match');
el.vsn = vsn;

% define data to collect % send
% default: file_sample_data = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS
% default: file_event_filter= LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON
% default: file_event_data   = GAZE,GAZERES,AREA,HREF,VELOCITY

if v ==3 && str2double(vsn{1}) == 4 % if EL 1000 and tracker version 4.xx
    
    % file:
    
    Eyelink('command', 'pupil_size_diameter = YES');	% no for pupil area (yes for dia; Eyelink('command', 'pupil_size_diameter = DIAMETER');)
    Eyelink('command', 'file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
    Eyelink('command', 'file_event_data   = GAZE,GAZERES,AREA,HREF,VELOCITY');     % SR defaults
    status = Eyelink('command', 'file_sample_data  = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS,INPUT');
    if status~=0
        status
        error('link_sample_data error')
    end
    % set link data (used for gaze cursor)
    Eyelink('command', 'link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,FIXUPDATE,INPUT');
    status=Eyelink('command', 'link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS,INPUT');
    if status~=0
        status
        error('link_sample_data error')
    end
    
elseif not(isempty(vs))
    fprintf('Wrong tracker or software version. Error at file/link data properties');
end

% define distance between participant and ET
Eyelink('command', 'simulation_screen_distance = 535');

% open file to record data to

edfFile = sprintf('S%sr%d.edf', subNo, irun);

% % TODO check if ET can handle more than 8 chars for filename
% edfFile = sprintf('S%d_%d_p%dr%d.edf', subNo, scanNo, phase, irun ); % datestr(now, 'mmdd')  

el.edfFile      = edfFile;
Eyelink('Openfile', edfFile);
Eyelink('command', 'add_file_preamble_text', strcat('Viewing', subNo, date));

% Specify calibration settings & calibrate the eye tracker

% This command is crucial to map the gaze positions from the tracker to
% screen pixel positions to determine fixation


Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, el.winWidth-1, el.winHeight-1);
Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, el.winWidth-1, el.winHeight-1);

% map drift target to fix cross position

el.driftx = el.winWidth/2;      %[x,y,z] =DrawFormattedText(win, '+', el.winWidth/2, el.winHeight/2); clear ('x','y','z')
el.drifty = el.winHeight/2;

% suggested by SR manual:
Eyelink('command','recording_parse_type = GAZE');
Eyelink('command','saccade_velocity_threshold = 30');
Eyelink('command','saccade_acceleration_threshold = 8000');
Eyelink('command','saccade_motion_threshold = 0.1');
Eyelink('command','saccade_pursuit_fixup = 60');
Eyelink('command','fixation_update_interval = 0');

Eyelink('command', 'calibration_type = HV5');           % use 5 vs 9 point matrix?
Eyelink('command', 'generate_default_targets = YES');

if strcmp(setup.ET.falsePupilRec, 'yes')
    [el] = falsePupilRec(el, win);
end

if not(setup.ET.ELdummymode)
    result = EyelinkDoTrackerSetup(el); % calibration etc?
    if result~=0
        result
        fprintf('Tracker setup failed (calibration/validation)!');
        sca;
    end
end

% save calibration
[result message] = Eyelink('CalMessage');
el.calibrationresult = result;
el.calibrationmessage = message;
Eyelink('Message', message);

%check which eye is being tracked: returns 0 (LEFT_EYE), 1 (RIGHT_EYE) or 2 (BINOCULAR) depending on what data is
el.eye_used = Eyelink('EyeAvailable'); % get eye that's tracked

% do a final check of calibration using driftcorrection -- enable correction(instead of simply checking)?:     Eyelink('command', 'driftcorrect_cr_disable = OFF');    el.applydriftcorr =  Eyelink('ApplyDriftCorr');
result=EyelinkDoDriftCorrection(el,800,457);  % , el.driftx, el.drifty;  success = 1;

if result~=1
    %cleanup;
    return;
end
[result, message] = Eyelink('CalMessage');
el.driftresult = result;
el.driftmessage = message;
Eyelink('Message', message)

% Deactivate Calibration Beeps during presentation
el.targetbeep=0;    % sound a beep when a target is presented?
el.feedbackbeep=0;  % sound a beep after calibration/drift correction?
EyelinkUpdateDefaults(el);

% start recording eye position
Eyelink('StartRecording');
% record a few samples before we actually start displaying
WaitSecs(0.1);

% mark zero-plot time in data file
Eyelink('Message', 'begin');

% check recording; terminate presentation if ET data is not recorded: Returns 0 if recording in progress
status=Eyelink('CheckRecording');           %initially used 'error' instead of status
if(status~=0)
    sca;
    display('Error: Eyetracking data is not recorded')
end

% re-activate keyboard for task!
ListenChar(0);

end