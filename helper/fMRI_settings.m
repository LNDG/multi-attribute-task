scanner = 1; % disable this to test the code outside the scanner

global t0

if scanner
    outportb(890,32) % sets  pin for tristate in base+2 to up state. This allows to read from the port
    %at the beginning of your script, include these lines: I never understood exactly what they do, but you
    %need them to be allowed to read data from the base address in the parallel port
end

if scanner
    scannerPort     = 888;
    triggerSwitches = 4;
    myPort = scannerPort;
    SCAN = [];
end

b = 1;  %this is the block (or scanner run). It's just useful to do sanity checks and make sure that the scanner triggering worked.

if scanner
    [SCAN t0] = fMRI_waitScannerTriggers(scannerPort, win, white, b, triggerSwitches, SCAN);
end