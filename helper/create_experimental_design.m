% TODO make function?
% function [outstruct] = EyeMem_expdesign(phase)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% experimental design (start)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         % TEST SETTINGS: 1 runs, 30 trials study, 60 test. 70 minutes
%         stim_category = {'naturals2'}; % 'fractals','landscapes','naturals1','naturals2','streets1','streets2',
%         num_study_run = 1; % number of runs in study phase
%         num_study_trial_run = 30; % trials per run in study phase
%         num_test_run = 1; % should be same as num_study_run
%         num_test_trial_run = 60; % % trials per run in testing phase

% EXPERIMENT SETTINGS: 6 runs, 30 trials study, 60 test. 60 minutes
%     stim_category = {'fractals','landscapes','naturals1','naturals2','streets1','streets2'};
%     stim_category = {'fractals','landscapes','naturals1','naturals2','streets1'}; % run with S1-10, nat1 and 2 have duplicates
stim_category = {'fractals','landscapes','naturals1','streets1', 'streets2'};

num_study_run = 5; % number of runs in study phase
num_study_trial_run = 30; % trials per run in study phase
num_test_run = 5; % should be same as num_study_run
num_test_trial_run = 60; % % trials per run in testing phase

%     % EXPERIMENT SETTINGS: 7 runs, 30 trials study, 60 test. 70 minutes
%     stim_category = {'fractals','landscapes','naturals1','naturals2','streets1','streets2','streets3'};
%     num_study_run = 7; % number of runs in study phase
%     num_study_trial_run = 30; % trials per run in study phase
%     num_test_run = 7; % should be same as num_study_run
%     num_test_trial_run = 60; % % trials per run in testing phase
%

total_stim_count = 0;
cat_file_count = 0;
category_padding = 0;

% get file names for stimuli
for c = 1:numel(stim_category)
    category(c).name = stim_category{c};
    category_padding = max(category_padding, numel(category(c).name));
    cat_dir_list = dir(fullfile(basepath, stimuli_folder, stim_category{c}));
    cat_dir_list = {cat_dir_list.name};
    category(c).test_file = {};
    % check if directory entries are images
    for s = 1:numel(cat_dir_list)
        if not(isempty(regexp(cat_dir_list{s},'\.(jpg|png|bmp)$')))
            category(c).test_file{end+1} = cat_dir_list{s};
            % fprintf('%s | %s\n',category(c).name,category(c).test_file{end})
        end
    end
end

% fprintf('\n')
% for c = 1:numel(stim_category)
%     fprintf('%s | %i\n',category(c).name, numel(category(c).file))
%     total_stim_count = total_stim_count + numel(category(c).file);
% end
% fprintf('\ntotal_stim_count: %i\n',total_stim_count)

%
% select randomized half of images of each category for study phase
%
for c = 1:numel(category)
    num_test_file = numel(category(c).test_file);
    num_study_file = floor(num_test_file/2);
    
    % Take odd pics for study 
    odd_idx = find(mod(1:num_test_file,2));
    shuffle_idx = randperm(num_study_file);
    study_file_idx = odd_idx(shuffle_idx);
    
%     % select study pics randomly from set
%     study_file_idx = randperm(num_test_file, num_study_file);
    
    category(c).study_file = {};
    category(c).study_file = category(c).test_file(study_file_idx);
    
    % complementary set to study_file_idx are lure stimuli for testing
    %         test_lure_file_idx = ones(1, num_study_trial_run);
    test_lure_file_idx = ones(1, num_test_trial_run); % looks better
    test_lure_file_idx(study_file_idx) = 0;
    test_lure_file_idx = find(test_lure_file_idx);
    category(c).test_lure_file = {};
    category(c).test_lure_file = category(c).test_file(test_lure_file_idx);
    
    % fprintf('%*s | %i\n', category_padding, category(c).name, numel(category(c).study_file))
end

% take category order from mat file based on subNo, also to be used in test
load(fullfile(basepath, 'helper', 'shuffled_condition_orders.mat'))
if subNo == 0 % testing
    category_idx = [1 2 3 4 5];
else
    category_idx = shuffled_condition_orders(subNo,:);
end

% % assign a randomized category order to runs of both phases
% category_idx = randperm(numel(category));



%%%
%%% assign stimuli for study phase
%%%

fprintf('study phase\n')
fprintf('===========\n\n')

for irun = 1:num_study_run
    c = category_idx(irun); % randomized category for current run
    study.run(irun).category = category(c).name;
    num_study_file = numel(category(c).study_file); % number of stimuli
    
    %         % pick with replacing, so stimuli can occur multiple times
    %         run_file_idx = randi(num_study_file,1,num_study_trial_run); % stimuli index
    
    %         % pick without replacing! Each pic only once.
    run_file_idx = randperm(num_study_trial_run); % stimuli index
    
    study.run(irun).view_file = category(c).study_file(run_file_idx); % stimuli
    
    % create numeric indices for lures and non-lures for current run
    lure_idx = randperm(num_study_trial_run, floor(num_study_trial_run/2));
    non_lure_idx = ones(1, num_study_trial_run);
    non_lure_idx(lure_idx) = 0;
    non_lure_idx = find(non_lure_idx);
    study.run(irun).lure_idx = lure_idx;
    study.run(irun).non_lure_idx = non_lure_idx;
    study.run(irun).quiz_file = cell(1,num_study_trial_run);
    
    % if quiz is a non-lure then assign the view image as quiz image
    for l = non_lure_idx
        study.run(irun).quiz_file(l) = study.run(irun).view_file(l);
    end
    
    % if quiz is a lure then assign a random image from the randomized
    % study images of that run but not the image presented in the trial
    
    for l = lure_idx
        non_lure_file = study.run(irun).view_file(l); % stimulus we just displayed
        lure_file_idx = not(strcmp(non_lure_file, study.run(irun).view_file)); % all other stimuli
        lure_file_pick = randperm(numel(lure_file_idx),1); % select a random lure by index
        lure_file = study.run(irun).view_file(lure_file_pick); % get name of lure file
        study.run(irun).quiz_file(l) = lure_file; % assign lure file
    end
    
    study.run(irun).correctresponse = ones(1,num_study_trial_run);
    study.run(irun).correctresponse(lure_idx) = 2; % is Yes, was the same pic, 2 = No, different pic
    
    fprintf('run %i | %*s | %i stimuli | %i trials | %i lure\n', irun, ...
        category_padding, study.run(irun).category, num_study_file, ...
        numel(study.run(irun).view_file), numel(lure_idx))
    
end

%%%
%%% assign stimuli for testing phase
%%%

fprintf('\ntesting phase\n')
fprintf('=============\n\n')

for irun = 1:num_test_run
    
    c = category_idx(irun); % randomized category for current run
    test.run(irun).category = category(c).name;
    
    % create numeric indices for lures and non-lures for current run
    %
    % non-lure: if stimulus has been presented in the study phase
    %     lure: if stimulus has not been presented in the study phase
    %
    
    % define which of the trials should be lures
    lure_idx = randperm(num_test_trial_run, floor(num_test_trial_run/2));
    non_lure_idx = zeros(1, num_test_trial_run);
    non_lure_idx(lure_idx) = 1;
    non_lure_idx = find(not(non_lure_idx));
    test.run(irun).lure_idx = lure_idx;
    test.run(irun).non_lure_idx = non_lure_idx;
    test.run(irun).test_file = cell(1,num_test_trial_run);
    
    % the stimuli that have actually been presented in the study phase
    % the runs have the same category in study and test
    % removes stimuli have been presented multiple times in the run
    test.run(irun).non_lure_file = unique(study.run(irun).view_file);
    %          shuffle non_lure_pick before the loop
    % if stimulus is a non-lure then choose an image from the non-lure list
    non_lure_pick = randperm(numel(test.run(irun).non_lure_file));
    ctr=0;
    for l = non_lure_idx
        ctr=ctr+1;
        %             non_lure_pick = randi(numel(test.run(irun).non_lure_file));
        non_lure_file = test.run(irun).non_lure_file(non_lure_pick(ctr));
        test.run(irun).test_file(l) = non_lure_file;
    end
    % ORI:
    %         % if stimulus is a non-lure then choose an image from the non-lure list
    %         for l = non_lure_idx
    %             non_lure_pick = randi(numel(test.run(irun).non_lure_file));
    %             non_lure_file = test.run(irun).non_lure_file(non_lure_pick);
    %             test.run(irun).test_file(l) = non_lure_file;
    %         end
    
    % if stimulus is a lure then choose an image from the lure list
    lure_file_pick = randperm(numel(category(c).test_lure_file));
    ctr=0;
    for l = lure_idx
        ctr=ctr+1;
        %             lure_file_pick = randperm(numel(category(c).test_lure_file),1);
        lure_file = category(c).test_lure_file(lure_file_pick(ctr));
        test.run(irun).test_file(l) = lure_file; % assign lure file
    end
    %         % ORI:
    %         for l = lure_idx
    %             lure_file_pick = randperm(numel(category(c).test_lure_file),1);
    %             lure_file = category(c).test_lure_file(lure_file_pick);
    %             test.run(irun).test_file(l) = lure_file; % assign lure file
    %         end
    
    num_test_file = numel(category(c).test_lure_file) + numel(test.run(irun).non_lure_file);
    
    test.run(irun).correctresponse = ones(1,num_test_trial_run);
    test.run(irun).correctresponse(test.run(irun).lure_idx) = 2; % 1 = old 2 = new
    
    fprintf('run %i | %*s | %i stimuli | %i trials | %i lure\n', irun, ...
        category_padding, test.run(irun).category, num_test_file, ...
        numel(test.run(irun).test_file), numel(lure_idx))
    
end

% save exp design to file, for retrieval just in case
if phase == 1
    outpathfile = fullfile(basepath, 'exp_design', sprintf('S%d_studydesign.mat', subNo) );
    fprintf('Saving experimental design to %s\n', outpathfile)
    save(outpathfile, 'study')
else    
    outpathfile = fullfile(basepath, 'exp_design', sprintf('S%d_testdesign.mat', subNo) );
    fprintf('Saving experimental design to %s\n', outpathfile)
    save(outpathfile, 'test')
end
fprintf('\n')

    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % experimental design (end)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%


