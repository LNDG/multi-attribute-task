function el = ET_terminate(el, path)  % TODO make a function out of this
%% terminate ET data collection and transfer files

Eyelink('Message', 'end');
% wait a while to record a few more samples
WaitSecs(0.1);

% finish up: stop recording eye-movements,
% close graphics window, close data file and shut down tracker
status=Eyelink('CheckRecording');
if(status==0)
    Eyelink('StopRecording');
end;
Eyelink('CloseFile');

edfFile = el.edfFile;

% add el to Output
Output.el         = el;

% download data file
try
    %         cd(strcat(pdat.root, 'Results\'));

    % path = '/Users/kloosterman/Dropbox/PROJECTS/Memory_Eyetracking/experiment/edf';
%     path = '/Users/sschmidt/Project/eye-mem/edf';

    cur_pwd = pwd; % current working directory
    
    path = fullfile(path, 'edf'); % put in edf folder
    if not(exist(path,'dir'))
      mkdir(path); % TODO move this to the start
    end
    cd(path); % TODO move this to the start
    %         cd(fullfile(path, 'Results\'));
    
    fprintf('Receiving data file ''%s''\n', edfFile );
    status=Eyelink('ReceiveFile', edfFile);
    WaitSecs(0.5);
    
    if status > 0
        fprintf('ReceiveFile status %d\n', status);
    end
    if 2==exist(edfFile, 'file')
        fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd );
    end
    chdir(cur_pwd) % return to prior working directory
catch rdf
    fprintf('Problem receiving data file ''%s''\n', edfFile );
    rdf;
end

% Shutdown Eyelink:
Eyelink('Shutdown');

% TODO give output file a better name
%In the current folder, rename edf file to current condition (Day):
%     movefile(strcat(pdat.root, '\results\', cell2mat(P.CODE),'.edf'),strcat(pdat.output,'.edf'))


